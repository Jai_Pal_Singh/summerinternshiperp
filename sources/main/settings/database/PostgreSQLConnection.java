/**
 * 
 */
package settings.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import settings.propertySheet.*;

/**
 * @author Joey
 *
 */
public class PostgreSQLConnection {
	static Connection conn ;
	public static Connection getConnection() throws SQLException{

		if(conn==null||conn.isClosed()){
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn = DriverManager
						.getConnection("jdbc:postgresql://"+PropertiesFile.getDBServer()+":"+PropertiesFile.getDBPort()+"/" +PropertiesFile.getDBName() ,
								PropertiesFile.getDBUser(), PropertiesFile.getDBPassword());
				System.out.println("Connected to "+ PropertiesFile.getDBName()+" on "+PropertiesFile.getDBServer());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return conn;
	}
public static void closeConnection() throws SQLException{
if(conn!=null)	conn.close();
}
}
