/**
 * 
 */
package postgreSQLDatabase.feePayment;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author manisha pc
 *
 */
public class Transaction {

	private ArrayList<Long> ref_no;
	private long reg_id;
	private int semester;
	private String category;
	private Date date;
	private long transaction_id;
	private boolean completed;
	private int year;
	private int round;

	/**
	 * @return the ref_no
	 */
	public ArrayList<Long> getRef_no() {
		return ref_no;
	}

	/**
	 * @param ref_no
	 *            the ref_no to set
	 */
	public void setRef_no(ArrayList<Long> ref_no) {
		this.ref_no = ref_no;
	}

	/**
	 * @return the reg_id
	 */
	public long getReg_id() {
		return reg_id;
	}

	/**
	 * @param reg_id
	 *            the reg_id to set
	 */
	public void setReg_id(long reg_id) {
		this.reg_id = reg_id;
	}

	/**
	 * @return the semester
	 */
	public int getSemester() {
		return semester;
	}

	/**
	 * @param semester
	 *            the semester to set
	 */
	public void setSemester(int semester) {
		this.semester = semester;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category.toUpperCase();
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the transaction_id
	 */
	public long getTransaction_id() {
		return transaction_id;
	}

	/**
	 * @param transaction_id
	 *            the transaction_id to set
	 */
	public void setTransaction_id(long transaction_id) {
		this.transaction_id = transaction_id;
	}

	/**
	 * @return the completed
	 */
	public boolean isCompleted() {
		return completed;
	}

	/**
	 * @param completed
	 *            the completed to set
	 */
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public boolean getCompleted(){
		return completed;
	}
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the round
	 */
	public int getRound() {
		return round;
	}

	/**
	 * @param round the round to set
	 */
	public void setRound(int round) {
		this.round = round;
	}
}

	