package actions.registrationFeePayment;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import postgreSQLDatabase.feePayment.Query;


/**
 * Servlet implementation class VerifyFeePayment
 */

@WebServlet(
		name="Verify Fee payment Servlet",
		urlPatterns={"/VerifyRegistrationFeePayment"}
	)
public class VerifyFeePayment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerifyFeePayment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		long ref_no=Long.parseLong(request.getParameter("ref_no"));
		long transaction_id = postgreSQLDatabase.feePayment.Query.verifyFeePayment(ref_no);
		
		boolean completed = postgreSQLDatabase.feePayment.Query.isVerifyFeeTransaction(ref_no);
		
	
	
		
		JSONObject message=new JSONObject();
		message.put("transaction_complete",completed);
		message.put("transaction_id",transaction_id);
	response.getWriter().write(message.toString());	
	}

}
