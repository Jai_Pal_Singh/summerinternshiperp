package actions.notifications;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import postgreSQLDatabase.notifications.Query;

/**
 * Servlet implementation class MarkAsRead
 */

@WebServlet(
		name="Mark as read Servlet",
		urlPatterns={"/MarkAsReadNotif"}
	)
public class MarkAsRead extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MarkAsRead() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		Long userid=Long.parseLong(request.getSession().getAttribute("erpId").toString());
		Long notif_id=Long.parseLong(request.getParameter("notif_id"));
		System.out.println(userid+notif_id);
		Query.markAsRead(userid,notif_id);
		
		
	}

}
