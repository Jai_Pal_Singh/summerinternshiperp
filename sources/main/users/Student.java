/**
 *  @author Anshula 
 */
package users;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.ds.common.PGObjectFactory;
import org.postgresql.util.PGobject;

import exceptions.IncorrectFormatException;
import utilities.StringFormatter;
import utilities.UniqueList;

/**
 * @author Anshula
 *
 */
public class Student {
	final String email_regex="[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+(\\.[a-zA-z0-9]*){1,4}";
	final String name_regex="[\\.a-zA-Z\\s]*$";
	final String word_regex="[a-zA-Z]*$";
	final String phone_regex="\\+?[0-9]{7,13}";
	final String number_regex="[0-9]{1,}";
	private UniqueList<String> parameters = new UniqueList<String>();
	final static int address_max_length = 100;
	final static int mobile_max_length = 13;
	final static int name_max_length = 50;

	private String allocated_category = "";
	ArrayList<String> allocated_category_list = new ArrayList<String>();
	private String allocated_rank;
	private boolean applied;
	private String category = "";
	
	private String batch;
	private int choice_no;

	private long csab_id;

	private Date date_of_birth;
	private String email = "";
	private Date entry_date;

	private String father_contact = "";
	private String father_name = "";
	private String first_name = "";
	private String gender = "";
	ArrayList<String> gender_list = new ArrayList<String>();
	private String guardian_address = "";
	private String guardian_contact = "";
	private String guardian_email = "";
	private String guardian_name = "";
	private String hostel = "";
	private boolean hosteller = false;
	private int jee_adv_rollno;
	private int jee_main_rollno;
	private String last_name = "";
	private String local_address = "";
	private String middle_name = "";
	private String mobile = "";
	private String mother_contact = "";
	private String mother_name = "";
	private String name = "";
	private String nationality;
	ArrayList<String> nationality_list = new ArrayList<String>();
	private String permanent_address = "";
	private String program_allocated = "";
	private boolean pwd;
	private String quota = "";
	private String rc_name;
	private long registration_id;
	private boolean reported;
	private String room;
	private int round;
	private int semester;
private String username;

	/**
 * @return the username
 */
public String getUsername() {
	return username;
}

/**
 * @param username the username to set
 */
public void setUsername(String username) {
	if(username!=null){
	this.username = username;
	parameters.add("username");
	}
}


	private String state_eligibility = "";
	public final static ArrayList<String> state_list=new ArrayList<String>() {{
		add("ANDAMANN AND NICOBAR ISLANDS");
		add("ANDHRA PRADESH");
		add("ARUNACHAL PRADESH");
		add("ASSAM");
		add("BIHAR");
		add("CHHATTISGARH");
		add("CHANDIGARH");
		add("DADRA AND NAGAR HAVELI");
		add("DELHI (NCT)");
		add("GOA");
		add("GUJARAT");
		add("HARYANA");
		add("HIMACHAL PRADESH");
		add("JAMMU AND KASHMIR");
		add("JHARKHAND");
		add("KARNATAKA");
		add("KERALA");
		add("LAKSHADWEEP");
		add("MADHYA PRADESH");
		add("MAHARASHTRA");
		add("MANIPUR");
		add("MEGHALAYA");
		add("MIZORAM");
		add("NAGALAND");
		add("ODISHA(ORISSA)");
		add("PUDUCHERRY");
		add("PUNJAB");
		add("RAJASTHAN");
		add("SIKKIM");
		add("TAMIL NADU");
		add("TELANGANA");
		add("TRIPURA");
		add("UTTAR PRADESH");
		add("UTTARAKHAND");
		add("WEST BENGAL");

	}};

	public final static ArrayList<String> program_allocated_list =  new ArrayList<String>() {{
		add("Computer Science and Engineering (4 Years Bachelor of Technology)".toUpperCase());
		add("Electronics and Communication Engineering (4 Years Bachelor of Technology)".toUpperCase());
		add("computer engineering".toUpperCase());
	}};
	public final static ArrayList<String> category_list=new ArrayList<String>(){/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		add("GENERAL");add( "OBC");add( "SC");add( "ST"); add("OBC-NCL");
	}
	};
	
	private String status = "";

	private String student_id;
	private int verification_status;
	private boolean verified;
	private String willingness = "";

	public static void main(String[] args) throws IncorrectFormatException {
		Student test=null;
		try {
			test = postgreSQLDatabase.registration.Query.getRegistrationStudentData(217l);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HashMap<String, String> map = test.getTagHashMap();
		Iterator<String> key_iterator = map.keySet().iterator();
		while(key_iterator.hasNext()){
			System.out.println(key_iterator.next());
		}


	}

	public Student() {
		gender_list.addAll(Arrays.asList("male", "female"));
		allocated_category_list.addAll(Arrays.asList("OPEN", "SC", "ST", "OBC-NCL", "OPEN(PWD)", "OBC-NCL(PWD)"));
		nationality_list.addAll(Arrays.asList("indian"));
	}

	public Student(ResultSet rs, boolean override_validate)	throws SQLException, IncorrectFormatException, ParseException {
		this();
		ResultSetMetaData meta = rs.getMetaData();
		for(int i=1;i<=meta.getColumnCount();i++){
			try{
				switch(meta.getColumnName(i)){

				case "name":
					setName(rs.getString(i));
					break;
				case "first_name":
					setFirst_name(rs.getString(i));
					break;
				case "middle_name":
					setMiddle_name(rs.getString(i));
					break;
				case "last_name":
					setLast_name(rs.getString(i));
					break;
				case "verification_status":
					setVerification_status(rs.getInt(i));
					break;
				case "category":
					setCategory(rs.getString(i));
					break;
				case "jee_adv_rollno":
					setJee_adv_rollno(rs.getString(i));
					break;
				case "jee_main_rollno":
					setJee_main_rollno(rs.getString(i));
					break;
				case "state":
					setState_eligibility(rs.getString(i));
					break;
				case "phone_number":
					setMobile(rs.getString(i));
					break;
				case "email":
					setEmail(rs.getString(i));
					break;
				case "date_of_birth":
					setDate_of_birth(rs.getString(i));
					break;
				case "program_allocated":
					setProgram_allocated(rs.getString(i));
					break;
				case "allocated_category":
					setAllocated_category(rs.getString(i));
					break;
				case "allocated_rank":
					setAllocated_rank(rs.getString(i));
					break;
				case "status":
					setStatus(rs.getString(i));
					break;
				case "choice_no":
					setChoice_no(rs.getInt(i));
					break;
				case "physically_disabled":
					setPwd(rs.getBoolean(i));
					break;
				case "gender":
					setGender(rs.getString(i));
					break;
				case "quota":
					setQuota(rs.getString(i));
					break;
				case "round":
					setRound(rs.getInt(i));
					break;
				case "willingness":
					setWillingness(rs.getString(i));
					break;
				case "address":
					setPermanent_address(rs.getString(i));
					break;
				case "rc_name":
					setRc_name(rs.getString(i));
					break;
				case "nationality":
					setNationality(rs.getString(i));
					break;
				case "id":
					setRegistration_id(rs.getLong(i));
					break;
				case "reg_id":
					setRegistration_id(rs.getLong(i));
					break;
				case "entry_date":
					setEntry_date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").parse(rs.getString(i)));
					break;
				case "verified":
					setVerified(rs.getBoolean(i));
					break;
				case "semester":
					setSemester(rs.getInt(i));
					break;
				case "permanent_address":
					setPermanent_address(rs.getString(i));
					break;
				
				case "local_address":
					setLocal_address(rs.getString(i));
					break;
				case "guardian_name":
					setGuardian_name(rs.getString(i));
					break;
				case "guardian_contact":
					setGuardian_contact(rs.getString(i));
					break;
				case "guardian_email":
					setGuardian_email(rs.getString(i));
					break;
				case "guardian_address":
					setGuardian_address(rs.getString(i));
					break;
				case "father_name":
					setFather_name(rs.getString(i));
					break;
				case "mother_name":
					setMother_name(rs.getString(i));
					break;
				case "father_contact":
					setFather_contact(rs.getString(i));
					break;
				case "mother_contact":
					setMother_contact(rs.getString(i));
					break;
				case "hosteller":
					setHosteller(rs.getBoolean(i));
					break;
				case "hostel_address":
					setHostel_address(rs.getString(i));
					break;
				case "csab_id":
					setCsab_id(rs.getLong(i));
					break;
				case "entry_time":
					setEntry_date(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").parse(rs.getString(i)));
					break;
				case "reported":
					setReported(rs.getBoolean(i));
					break;
				case "student_id":
					setStudent_id(rs.getString(i));
					break;
				case "applied":
					setApplied(rs.getBoolean(i));
					break;
				case "registration_id":
					setRegistration_id(rs.getLong(i));
					break;
				case "username":
					setUsername(rs.getString(i));
					break;
				default:
					if(settings.Execution.debug_mode)	System.out.println("Unknown column " + meta.getColumnName(i));
					break;

				}
			} catch (NullPointerException e) {
				if(settings.Execution.debug_mode) System.out.println("empty value in " + meta.getColumnName(i));
			} catch (JSONException e) {
				if(settings.Execution.debug_mode)	System.out.println(e.getMessage());
			}
			catch (IncorrectFormatException e){
				if(settings.Execution.debug_mode)System.out.println("Incorrect format overidden");
				if(override_validate) ;

				else throw e;	
			}

		}

	}

	public Student(ResultSet rs) throws SQLException, IncorrectFormatException, ParseException {
		this(rs, true);
	}
	public JSONObject getHostel_address(){
		JSONObject jObject = new JSONObject();
		jObject.put("hostel", getHostel());
		jObject.put("room", getRoom());
		return jObject;
	}
	/**
	 * @return the allocated_category
	 */
	public String getAllocated_category() {
		return allocated_category;
	}

	/**
	 * @return the allocated_rank
	 */
	public String getAllocated_rank() {
		return allocated_rank;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @return the choice_no
	 */
	public int getChoice_no() {
		return choice_no;
	}

	/**
	 * @return the csab_id
	 */
	public long getCsab_id() {
		return csab_id;
	}

	/**
	 * @return the date_of_birth
	 */
	public Date getDate_of_birth() {
		return date_of_birth;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the entry_date
	 */
	public Date getEntry_date() {
		return entry_date;
	}

	/**
	 * @return the father_contact
	 */
	public String getFather_contact() {
		return father_contact;
	}

	/**
	 * @return the father_name
	 */
	public String getFather_name() {
		return father_name;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @return the guardian_address
	 */
	public String getGuardian_address() {
		return guardian_address;
	}

	/**
	 * @return the guardian_contact
	 */
	public String getGuardian_contact() {
		return guardian_contact;
	}

	/**
	 * @return the guardian_email
	 */
	public String getGuardian_email() {
		return guardian_email;
	}

	/**
	 * @return the guardian_name
	 */
	public String getGuardian_name() {
		return guardian_name;
	}

	/**
	 * @return the hostel
	 */
	public String getHostel() {
		return hostel;
	}

	/**
	 * @return the jee_adv_rollno
	 */
	public int getJee_adv_rollno() {
		return jee_adv_rollno;
	}

	/**
	 * @return the jee_main_rollno
	 */
	public int getJee_main_rollno() {
		return jee_main_rollno;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @return the local_address
	 */
	public String getLocal_address() {
		return local_address;
	}

	/**
	 * @return the middle_name
	 */
	public String getMiddle_name() {
		return middle_name;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @return the mother_contact
	 */
	public String getMother_contact() {
		return mother_contact;
	}

	/**
	 * @return the mother_name
	 */
	public String getMother_name() {
		return mother_name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @return the permanent_address
	 */
	public String getPermanent_address() {
		return permanent_address;
	}

	/**
	 * @return the program_allocated
	 */
	public String getProgram_allocated() {
		return program_allocated;
	}

	/**
	 * @return the quota
	 */
	public String getQuota() {
		return quota;
	}

	/**
	 * @return the rc_name
	 */
	public String getRc_name() {
		return rc_name;
	}

	/**
	 * @return the registration_id
	 */
	public long getRegistration_id() {
		return registration_id;
	}

	/**
	 * @return the room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * @return the round
	 */
	public int getRound() {
		return round;
	}

	/**
	 * @return the semester
	 */
	public int getSemester() {
		return semester;
	}

	/**
	 * @return the state_eligibility
	 */
	public String getState_eligibility() {
		return state_eligibility;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the student_id
	 */
	public String getStudent_id() {
		if(student_id==null||student_id.equals(""))return postgreSQLDatabase.registration.Query.getStudentId(this.registration_id);
		return student_id;
	}

	/**
	 * @return the verification_status
	 */
	public int getVerification_status() {
		return verification_status;
	}

	public boolean getVerified() {
		return verified;
	}

	/**
	 * @return the willingness
	 */
	public String getWillingness() {
		return willingness;
	}

	/**
	 * @return the applied
	 */
	public boolean isApplied() {
		return applied;
	}

	/**
	 * @return the hosteller
	 */
	public boolean isHosteller() {
		return hosteller;
	}

	/**
	 * @return the pwd
	 */
	public boolean isPwd() {
		return pwd;
	}

	/**
	 * @return the reported
	 */
	public boolean isReported() {
		return reported;
	}

	/**
	 * @param allocated_category
	 *            the allocated_category to set
	 */
	public void setAllocated_category(String allocated_category) throws IncorrectFormatException {
		if (allocated_category_list.contains(allocated_category.toUpperCase()))
			this.allocated_category = allocated_category.toUpperCase();
		else
			throw new IncorrectFormatException("Incorrect Category exception " + allocated_category );
		parameters.add("allocated_category");

	}

	/**
	 * @param allocated_rank
	 *            the allocated_rank to set
	 */
	public void setAllocated_rank(String allocated_rank) {
		this.allocated_rank = allocated_rank;
		parameters.add("allocated_rank");
	}

	/**
	 * @param applied
	 *            the applied to set
	 */
	public void setApplied(boolean applied) {
		this.applied = applied;
		parameters.add("applied");
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) throws IncorrectFormatException {
		if (category_list.contains(category.toUpperCase())){
			this.category = category.toUpperCase();
			parameters.add("category");
		}

		else
			throw new IncorrectFormatException("Incorrect category exception " + category);

	}

	/**
	 * @param choice_no
	 *            the choice_no to set
	 */
	public void setChoice_no(int choice_no) {
		this.choice_no = choice_no;
		parameters.add("choice_no");
	}
	public void setChoice_no(String choice_no) throws IncorrectFormatException {
		try{
		this.choice_no = Integer.parseInt(choice_no);
		parameters.add("choice_no");
		}catch(Exception e){
			throw new IncorrectFormatException("Incorrect Choice Number " +choice_no);
		}
		
	}

	public void setCsab_id(long csab_id) {
		this.csab_id = csab_id;
		parameters.add("csab_id");
	}

	/**
	 * @param date_of_birth
	 *            the date_of_birth to set
	 */
	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
		parameters.add("date_of_birth");
	}

	/**
	 * @param date_of_birth
	 *            the date_of_birth to set
	 */
	public void setDate_of_birth(String date_of_birth) throws IncorrectFormatException {

		
		SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat df4 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date;
				try {
					date = df3.parse(date_of_birth);
					
					this.date_of_birth = utilities.StringFormatter.convert(date);
					parameters.add("date_of_birth");
				} catch (ParseException e4) {
					try {
						date = df4.parse(date_of_birth);
						
						this.date_of_birth = utilities.StringFormatter.convert(date);
						parameters.add("date_of_birth");
					}
					catch (ParseException e5){
					e5.printStackTrace();
						throw new IncorrectFormatException("Unparsable date "+ date_of_birth);
					}
					}
				}
			
		

	

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) throws IncorrectFormatException {
		String expression = email_regex;
		if (email==null)throw new IncorrectFormatException("Incorrect Personal Email Format " + email);
		else if	(email.matches(expression)) {
			this.email = email;
			parameters.add("email");
		} else {
			throw new IncorrectFormatException("Incorrect Personal Email Format " + email);
		}

	}

	/**
	 * @param entry_date
	 *            the entry_date to set
	 */
	public void setEntry_date(Date entry_date) {
		this.entry_date = entry_date;
		parameters.add("entry_date");
	}

	/**
	 * @param father_contact
	 *            the father_contact to set
	 * @throws IncorrectFormatException
	 */
	public void setFather_contact(String father_contact) throws IncorrectFormatException {
		if (father_contact.matches(phone_regex)) {
			this.father_contact = father_contact;

			parameters.add("father_contact");
		}

		else {
			throw new IncorrectFormatException("Incorrect Father Contact Number " + father_contact);
		}
	}

	/**
	 * @param father_name
	 *            the father_name to set
	 * @throws IncorrectFormatException
	 */
	public void setFather_name(String father_name) throws IncorrectFormatException {


		if (father_name.matches(name_regex)) {
			this.father_name = father_name.toUpperCase();
			parameters.add("father_name");
		} else {
			throw new IncorrectFormatException("Incorrect Name Format " + father_name);
		}
	}

	/**
	 * @param first_name
	 *            the first_name to set
	 * @throws IncorrectFormatException
	 */
	public void setFirst_name(String first_name) throws IncorrectFormatException {
		first_name=first_name.trim();
		if (first_name != null && !first_name.equals("null")) {
			if (first_name.matches(word_regex) && first_name.length() <= name_max_length) {

				this.first_name = first_name.toUpperCase();
				parameters.add("first_name");
			} else
				throw new IncorrectFormatException("Incorrect Name Format " + first_name);
		}

		else
			this.first_name = "";
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) throws IncorrectFormatException {
		if (gender_list.contains(gender.toLowerCase())) {
			this.gender = gender;
			parameters.add("gender");
		} else
			throw new IncorrectFormatException("Incorrect Gender " + gender);

	}

	/**
	 * @param guardian_address
	 *            the guardian_address to set
	 */
	public void setGuardian_address(String guardian_address) {
		this.guardian_address = guardian_address;
		parameters.add("guardian_address");
	}

	/**
	 * @param guardian_contact
	 *            the guardian_contact to set
	 * @throws IncorrectFormatException
	 */
	public void setGuardian_contact(String guardian_contact) throws IncorrectFormatException {
		 if (guardian_contact.matches(phone_regex)) {
			this.guardian_contact = guardian_contact;
			parameters.add("guardian_contact");
		} else {
			throw new IncorrectFormatException("Incorrect Contact Number " + guardian_contact);
		}
	}

	/**
	 * @param guardian_email
	 *            the guardian_email to set
	 * @throws IncorrectFormatException
	 */
	public void setGuardian_email(String guardian_email) throws IncorrectFormatException {
		if (guardian_email.matches(email_regex)) {
			this.guardian_email = guardian_email;
			parameters.add("guardian_email");
		}

		else
			throw new IncorrectFormatException("Incorrect Guardian Email " + guardian_email);
	}

	/**
	 * @param guardian_name
	 *            the guardian_name to set
	 * @throws IncorrectFormatException
	 */
	public void setGuardian_name(String guardian_name) throws IncorrectFormatException {
		 if (guardian_name.matches(name_regex) && guardian_name.length() <= name_max_length) {
			this.guardian_name = StringFormatter.TitleCase(guardian_name);
			parameters.add("guardian_name");
		} else
			throw new IncorrectFormatException("Incorrect Guardian Name Format " + guardian_name);
	}

	/**
	 * @param hostel
	 *            the hostel to set
	 */
	public void setHostel(String hostel) {
		if(hostel==null);
		else {
			this.hostel = hostel;
			parameters.add("hostel");

		}
			}

	/**
	 * @param hosteller
	 *            the hosteller to set
	 */
	public void setHosteller(boolean hosteller) {
		this.hosteller = hosteller;
		parameters.add("hosteller");
	}
	public void setHostel_address(String json){
		JSONObject jObject=new JSONObject(json);
		try{
			jObject=new JSONObject(json);
		}catch(Exception e){

		}
		try{
			setHostel(jObject.getString("hostel"));
			parameters.add("hostel_address");
		}catch(Exception e){}
		try{
			setRoom(jObject.getString("room"));
			parameters.add("hostel_address");
		}catch(Exception e){}
	}

	/**
	 * @param jee_adv_rollno
	 *            the jee_adv_rollno to set
	 */
	public void setJee_adv_rollno(String jee_adv_rollno) throws IncorrectFormatException {
		if (jee_adv_rollno.matches(number_regex)) {
			this.jee_adv_rollno = Integer.parseInt(jee_adv_rollno);
			parameters.add("jee_adv_rollno");
		} 
		else if(jee_adv_rollno.equals("")||jee_adv_rollno.equals("--")){
			this.jee_adv_rollno = 0;
		}
		else {
			throw new IncorrectFormatException("Incorrect JEE Advance Roll Number  " + jee_adv_rollno);
		}

	}

	/**
	 * @param jee_main_rollno
	 *            the jee_main_rollno to set
	 * @throws IncorrectFormatException
	 */
	public void setJee_main_rollno(String jee_main_rollno) throws IncorrectFormatException {

		if (jee_main_rollno.matches(number_regex)) {
			this.jee_main_rollno = Integer.parseInt(jee_main_rollno);
			parameters.add("jee_main_rollno");
		} else {
			throw new IncorrectFormatException("Incorrect JEE Main Roll Number Number " + jee_main_rollno );
		}

	}

	/**
	 * @param last_name
	 *            the last_name to set
	 * @throws IncorrectFormatException
	 */
	
	

	public void setLast_name(String last_name) throws IncorrectFormatException {
		if (last_name != null && !last_name.equals("null")) {
			if (last_name.matches(word_regex) && last_name.length()<name_max_length) {

				this.last_name = last_name.trim().toUpperCase();
				parameters.add("last_name");
			} else
				throw new IncorrectFormatException("Incorrect Last Name Format " + last_name );
		}

		else
			this.last_name = "";
	}

	/**
	 * @param local_address
	 *            the local_address to set
	 */
	public void setLocal_address(String local_address) {
		this.local_address = local_address.toUpperCase();
		parameters.add("local_address");
	}

	/**
	 * @param middle_name
	 *            the middle_name to set
	 * @throws IncorrectFormatException
	 */
	public void setMiddle_name(String middle_name) throws IncorrectFormatException {
		if (middle_name != null && !middle_name.equals("null")) {
			if (middle_name.matches(word_regex) && middle_name.length() <= name_max_length) {

				this.middle_name = middle_name.trim().toUpperCase();
				parameters.add("middle_name");
			} else
				throw new IncorrectFormatException("Incorrect Middle Name Format " + middle_name );
		}

		else
			this.middle_name = "";
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) throws IncorrectFormatException {
		if (mobile.matches(phone_regex)) {
			this.mobile = mobile;
		} else {
			this.mobile = "";

			throw new IncorrectFormatException("Incorrect Personal Phone Number " + mobile);
		}
		parameters.add("phone_number");
	}

	/**
	 * @param mother_contact
	 *            the mother_contact to set
	 * @throws IncorrectFormatException
	 */
	public void setMother_contact(String mother_contact) throws IncorrectFormatException {
		if (mother_contact.matches(phone_regex)) {
			this.mother_contact = mother_contact;
			parameters.add("mother_contact");
		} else {
			throw new IncorrectFormatException("Incorrect Mother Contact Number " + mother_contact );
		}
	}

	/**
	 * @param mother_name
	 *            the mother_name to set
	 * @throws IncorrectFormatException
	 */
	public void setMother_name(String mother_name) throws IncorrectFormatException {
		if (mother_name.matches(name_regex) && mother_name.length() <= name_max_length) {
			this.mother_name = mother_name.toUpperCase();
			parameters.add("mother_name");
		} else {
			throw new IncorrectFormatException("Incorrect Mother Name Format " + mother_name );
		}
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) throws IncorrectFormatException {
		name=name.trim();
		if (name.matches(name_regex) && name.length() <= name_max_length) {
			this.name = StringFormatter.TitleCase(name);
			parameters.add("name");
		} else {
			throw new IncorrectFormatException("Incorrect Name Format " + name );
		}
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) throws IncorrectFormatException {
		if (nationality_list.contains(nationality.toLowerCase()))
			this.nationality = nationality;
		else
			throw new IncorrectFormatException("Incorrect Nationality " + nationality );
		parameters.add("nationality");
	}

	/**
	 * @param permanent_address
	 *            the permanent_address to set
	 */
	public void setPermanent_address(String permanent_address) {
		this.permanent_address = permanent_address.toUpperCase();
		parameters.add("permanent_address");
		parameters.add("address");
	}

	/**
	 * @param program_allocated
	 *            the program_allocated to set
	 */
	public void setProgram_allocated(String program_allocated) throws IncorrectFormatException {
		
		if (program_allocated_list.contains(program_allocated.toUpperCase()))
			this.program_allocated = program_allocated.toUpperCase();
		else
			throw new IncorrectFormatException("Incorrect Program Allocated Format '" + program_allocated +"'");
		parameters.add("program_allocated");
	}

	/**
	 * @param pwd
	 *            the pwd to set
	 */
	public void setPwd(boolean pwd) {
		this.pwd = pwd;
		parameters.add("pwd");
	}

	/**
	 * @param pwd
	 *            the pwd to set
	 */
	public void setPwd(String pwd) {
		if (pwd.equalsIgnoreCase("yes"))
			this.pwd = true;
		else
			this.pwd = false;
		parameters.add("pwd");
	}

	/**
	 * @param quota
	 *            the quota to set
	 */
	public void setQuota(String quota) {
		this.quota = quota;
		parameters.add("quota");
	}

	/**
	 * @param rc_name
	 *            the rc_name to set
	 */
	public void setRc_name(String rc_name) {
		this.rc_name = rc_name;
		parameters.add("rc_name");
	}

	/**
	 * @param registration_id
	 *            the registration_id to set
	 */
	public void setRegistration_id(long registration_id) {
		this.registration_id = registration_id;
		parameters.add("registration_id");
		parameters.add("profile_picture");
	}

	/**
	 * @param reported
	 *            the reported to set
	 */
	public void setReported(boolean reported) {
		this.reported = reported;
		parameters.add("reported");
	}

	/**
	 * @param room
	 *            the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
		parameters.add("room");
	}

	/**
	 * @param round
	 *            the round to set
	 */
	public void setRound(int round) {
		this.round = round;
		parameters.add("round");
	}
	public void setRound(String round) throws IncorrectFormatException {
		try{
		this.round = Integer.parseInt(round) ;
		parameters.add("round");
		}
		catch(NumberFormatException e){
			throw new IncorrectFormatException("Incorrect Round Format" + round );
		}
	}

	/**
	 * @param semester
	 *            the semester to set
	 */
	public void setSemester(int semester) {
		this.semester = semester;
		parameters.add("semester");
	}

	/**
	 * @param state_eligibility
	 *            the state_eligibility to set
	 */
	public void setState_eligibility(String state_eligibility) throws IncorrectFormatException {

		// if(state_list.contains(state_eligibility.toLowerCase()))
		this.state_eligibility = state_eligibility;
		// else
		// throw new IncorrectFormatException("state");

		if (state_list.contains(state_eligibility.toUpperCase())) {
			this.state_eligibility = state_eligibility;
			parameters.add("state_eligibility");
		} else
			throw new IncorrectFormatException("Incorrect State Name " + state_eligibility );

	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
		parameters.add("status");
	}

	/**
	 * @param student_id
	 *            the student_id to set
	 */
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
		parameters.add("student_id");
	}

	/**
	 * @param verification_status
	 *            the verification_status to set
	 */
	public void setVerification_status(int verification_status) {
		this.verification_status = verification_status;
		parameters.add("verification_status");

	}

	/**
	 * @param csab_id
	 *            the csab_id to set
	 */
	public void setVerified(boolean verified) {
		this.verified = verified;
		parameters.add("verified");
	}

	/**
	 * @param willingness
	 *            the willingness to set
	 */
	public void setWillingness(String willingness) {
		this.willingness = willingness;
		parameters.add("willingness");
	}

	/**
	 * @return the batch
	 */
	public String getBatch() {
		return batch;
	}

	/**
	 * @param batch
	 *            the batch to set
	 */
	public void setBatch(String batch) {
		this.batch = batch;
		parameters.add("batch");
	}

	public JSONObject getJSON() {
		Iterator<String> iterator = parameters.iterator();
		JSONObject student = new JSONObject();
		while (iterator.hasNext()) {
			String parameter = iterator.next();
			switch (parameter) {

			case "name":
				student.put(parameter, getName());
				break;
			case "first_name":
				student.put("first_name", getFirst_name());
				break;
			case "middle_name":
				student.put("middle_name", getMiddle_name());
				break;
			case "last_name":
				student.put("last_name", getLast_name());
				break;
			case "verification_status":
				student.put("verification_status", getVerification_status());
				break;
			case "category":
				student.put("category", getCategory());
				break;
			case "jee_adv_rollno":
				student.put("jee_adv_rollno", getJee_adv_rollno());
				break;
			case "jee_main_rollno":
				student.put("jee_main_rollno", getJee_main_rollno());
				break;
			case "state_eligibility":
				student.put("state", getState_eligibility());
				break;
			case "phone_number":
				student.put("phone_number", getMobile());
				break;
			case "email":
				student.put("email", getEmail());
				break;
			case "date_of_birth":
				student.put("date_of_birth", getDate_of_birth());
				break;
			case "program_allocated":
				student.put("program_allocated", getProgram_allocated());
				break;
			case "allocated_category":
				student.put("allocated_category", getAllocated_category());
				break;
			case "allocated_rank":
				student.put("allocated_rank", getAllocated_rank());
				break;
			case "status":
				student.put("status", getStatus());
				break;
			case "choice_no":
				student.put("choice_no", getChoice_no());
				break;
			case "pwd":
				student.put("physically_disabled", isPwd());
				break;
			case "gender":
				student.put("gender", getGender());
				break;
			case "quota":
				student.put("quota", getQuota());
				break;
			case "round":
				student.put("round", getRound());
				break;
			case "willingness":
				student.put("willingness", getWillingness());
				break;
			case "address":
				student.put("address", getPermanent_address());
				break;
			case "rc_name":
				student.put("rc_name", getRc_name());
				break;
			case "nationality":
				student.put("nationality", getNationality());
				break;
			case "id":
				student.put("id", getRegistration_id());
				break;
			case "entry_date":
				student.put("entry_date",getEntry_date());
				break;
			case "verified":
				student.put("verified", getVerified());
				break;
			case "semester":
				student.put("semester", getSemester());
				break;
			case "permanent_address":
				student.put("permanent_address", getPermanent_address());
				break;
			case "local_address":
				student.put("local_address", getLocal_address());
				break;
			case "guardian_name":
				student.put("guardian_name", getGuardian_name());
				break;
			case "guardian_contact":
				student.put("guardian_contact", getGuardian_contact());
				break;
			case "guardian_email":
				student.put("guardian_email", getGuardian_email());
				break;
			case "guardian_address":
				student.put("guardian_address", getGuardian_address());
				break;
			case "father_name":
				student.put("father_name", getFather_name());
				break;
			case "mother_name":
				student.put("mother_name", getMother_name());
				break;
			case "father_contact":
				student.put("father_contact", getFather_contact());
				break;
			case "mother_contact":
				student.put("mother_contact", getMother_contact());
				break;
			case "hosteller":
				student.put("hosteller", isHosteller());
				break;
			case "hostel_address":
				student.put("hostel", getHostel());
				student.put("room", getRoom());
				break;
			case "csab_id":
				student.put("csab_id", getCsab_id());
				break;
			case "reported":
				student.put("reported", isReported());
				break;
			case "student_id":
				student.put("student_id", getStudent_id());
				break;
			case "applied":
				student.put("applied", isApplied());
				break;
			case "registration_id":
				student.put("registration_id", getRegistration_id());
				break;

			}
		}
		return student;

	}

	
	public HashMap<String, String> getTagHashMap() {
		Iterator<String> iterator = parameters.iterator();
		HashMap<String, String> student=new HashMap<>();
		while (iterator.hasNext()) {
			String parameter = iterator.next();
			switch (parameter) {

			case "name":
				student.put("{(student_"+parameter+")}", getName());
				break;
			case "first_name":
				student.put("{(student_"+parameter+")}", getFirst_name());
				break;
			case "middle_name":
				student.put("{(student_"+parameter+")}", getMiddle_name());
				break;
			case "last_name":
				student.put("{(student_"+parameter+")}", getLast_name());
				break;
			case "verification_status":
				student.put("{(student_"+parameter+")}", String.valueOf(getVerification_status()));
				break;
			case "category":
				student.put("{(student_"+parameter+")}", getCategory());
				break;
			case "jee_adv_rollno":
				student.put("{(student_"+parameter+")}",String.valueOf(getJee_adv_rollno()));
				break;
			case "jee_main_rollno":
				student.put("{(student_"+parameter+")}", String.valueOf(getJee_main_rollno()));
				break;
			case "state_eligibility":
				student.put("{(student_"+parameter+")}", getState_eligibility());
				break;
			case "phone_number":
				student.put("{(student_"+parameter+")}", getMobile());
				break;
			case "email":
				student.put("{(student_"+parameter+")}", getEmail());
				break;
			case "date_of_birth":
				student.put("{(student_"+parameter+")}", String.valueOf(getDate_of_birth()));
				break;
			case "program_allocated":
				student.put("{(student_"+parameter+")}", getProgram_allocated());
				break;
			case "allocated_category":
				student.put("{(student_"+parameter+")}", getAllocated_category());
				break;
			case "allocated_rank":
				student.put("{(student_"+parameter+")}", getAllocated_rank());
				break;
			case "status":
				student.put("{(student_"+parameter+")}", getStatus());
				break;
			case "choice_no":
				student.put("{(student_"+parameter+")}", String.valueOf(getChoice_no()));
				break;
			case "pwd":
				student.put("{(student_"+parameter+")}", String.valueOf(isPwd()));
				break;
			case "gender":
				student.put("{(student_"+parameter+")}", getGender());
                 if(getGender().equalsIgnoreCase("male")){
                	 student.put("{(student_poss_cap)}", "His");
                	 student.put("{(student_poss)}", "his");
                	 student.put("{(student_pro_cap)}", "He");
                	 student.put("{(student_pro)}", "he");
                	 student.put("{(student_child_of_cap)}", "S/o");
                	 student.put("{(student_child_of)}", "s/o");
                	 student.put("{(student_third_person_cap)}", "Him");
                	 student.put("{(student_third_person)}", "him");
                 }
                 else{
                	 student.put("{(student_poss_cap)}", "Her");
                	 student.put("{(student_poss)}", "her");
                	 student.put("{(student_pro_cap)}", "She");
                	 student.put("{(student_pro)}", "she");
                	 student.put("{(student_child_of_cap)}", "D/o");
                	 student.put("{(student_child_of)}", "d/o");
                	 student.put("{(student_third_person_cap)}", "Her");
                	 student.put("{(student_third_person)}", "her");
                 }
				break;
			case "quota":
				student.put("{(student_"+parameter+")}", getQuota());
				break;
			case "round":
				student.put("{(student_"+parameter+")}", String.valueOf(getRound()));
				break;
			case "willingness":
				student.put("{(student_"+parameter+")}", getWillingness());
				break;
			case "address":
				student.put("{(student_"+parameter+")}", getPermanent_address());
				break;

			case "rc_name":
				student.put("{(student_"+parameter+")}", getRc_name());
				break;
			case "nationality":
				student.put("{(student_"+parameter+")}", getNationality());
				break;
			case "id":
				student.put("{(student_"+parameter+")}", String.valueOf(getRegistration_id()));
				break;
			case "entry_date":
				student.put("{(student_"+parameter+")}",String.valueOf(getEntry_date()));
				break;
			case "verified":
				student.put("{(student_"+parameter+")}", String.valueOf(getVerified()));
				break;
			case "semester":
				student.put("{(student_"+parameter+")}", String.valueOf(getSemester()));
				break;
			case "permanent_address":
				student.put("{(student_"+parameter+")}", getPermanent_address());
				break;
			case "local_address":
				student.put("{(student_"+parameter+")}", getLocal_address());
				break;
			case "guardian_name":
				student.put("{(student_"+parameter+")}", getGuardian_name());
				break;
			case "guardian_contact":
				student.put("{(student_"+parameter+")}", getGuardian_contact());
				break;
			case "guardian_email":
				student.put("{(student_"+parameter+")}", getGuardian_email());
				break;
			case "guardian_address":
				student.put("{(student_"+parameter+")}", getGuardian_address());
				break;
			case "father_name":
				student.put("{(student_"+parameter+")}", getFather_name());
				break;
			case "mother_name":
				student.put("{(student_"+parameter+")}", getMother_name());
				break;
			case "father_contact":
				student.put("{(student_"+parameter+")}", getFather_contact());
				break;
			case "mother_contact":
				student.put("{(student_"+parameter+")}", getMother_contact());
				break;
			case "hosteller":
				student.put("{(student_"+parameter+")}", utilities.StringFormatter.booleanToString(isHosteller()));
				break;
			case "hostel_address":
				student.put("{(student_hostel_address)}", getHostel());
				student.put("{(student_hostel_room)}", getRoom());
				break;
			case "csab_id":
				student.put("{(student_"+parameter+")}", String.valueOf(getCsab_id()));
				break;
			case "reported":
				student.put("{(student_"+parameter+")}", String.valueOf(isReported()));
				break;
			case "student_id":
				student.put("{(student_id)}", getStudent_id());
				break;
			case "applied":
				student.put("{(student_"+parameter+")}", String.valueOf(isApplied()));
				break;
			case "registration_id":
				student.put("{(student_"+parameter+")}", String.valueOf(getRegistration_id()));
				break;
			case "username":
				student.put("{(student_"+parameter+")}", String.valueOf(getUsername()));
				break;
			case "profile_picture":
				try {
					student.put("{(student_"+parameter+")}", "http://"+settings.propertySheet.PropertiesFile.getFTPServer()+":8080/ftp/id_card_images/image_"+String.valueOf(getRegistration_id()+".png"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			

			}
		}
		return student;

	}

}
