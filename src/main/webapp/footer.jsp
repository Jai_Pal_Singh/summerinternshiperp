 <style>
 	.box{
 		border:none;
 	}
 </style>
   <script src="../dist/js/chats.js"></script>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong><span style="text-decoration;underline;cursor:pointer;" data-toggle="modal" data-target="#about-us">About Us</span> | Copyright &copy; 2016 <a href="http://iiitkota.ac.in">IIITK</a>.</strong> All rights
    reserved.
  </footer>
  <div class="modal fade" id="about-us" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
					<div class="modal-content">
  	<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">About Us</h4>
						</div>
  
  
	<div class="row" style="margin:1%;">
        <div class="col-xs-12">
          
		  <div class="box">
            <div class="box-body">
              <table id="example1" class="table">
              <caption>Project Manager</caption>
                <tbody>
                	<tr>
                		<td>
                			<br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			Joey Pinto
                		</td>
                	</tr>                			
                </tbody>
              </table>
              <table id="example1" class="table">
                <caption>UI/UX</caption>
                <tbody>
                	<tr>
                		<td><br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Sumit Kumar Sagar</span><br>
                			<designation>Head</designation>
                		</td>
                	</tr>                			
                </tbody>
              </table>
              <table id="example1" class="table">
              <caption>Database</caption>
                <tbody>
                	<tr>
                		<td><br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Megha Gupta</span><br>
                			<designation>Manager</designation>
                		</td>
                	</tr>                			
                </tbody>
              </table>
              <table id="example1" class="table">
              <caption>Java Development</caption>
                <tbody>
                	<tr>
                		<td><br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Manisha Ghugharwal</span><br>
                			<designation>Senior Developer</designation>
                		</td>
                		<td><br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Anita Gadhwal</span><br>
                			<designation>Senior Developer</designation>
                		</td>
                		<td><br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Arushi Gupta</span><br>
                			<designation>Senior Developer</designation>
                		</td>
                	</tr>                			
                </tbody>
              </table>
              <table id="example1" class="table">
              <caption>File System Management</caption>
                <tbody>
                	<tr>
                		<td><br>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Shivani Garg</span><br>
                			<designation>Manager</designation>
                		</td>
                	</tr>                			
                </tbody>
              </table>
              <table id="example1" class="table">
              <caption>Faculty Coordinators</caption>
                <tbody>
                	<tr>
                		<td>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Dr. Tapan Kumar Jain</span>
                		</td>
                		<td>
                			<img src="dist/img/default.png" class="img-circle"/><br>
                			<span>Dr. Pooja Jain</sapn>
                		</td>
                	</tr>                			
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      </div>
     </div> 
</div>
