<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="users.Student"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIIT KOTA | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="../plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<script
	src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.9.0/validate.min.js"></script>
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<!-- jvectormap -->
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script>
	var constraints = 

			

			{

				name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid first name", {
										text : value
									});
						}
					}
				},
				first_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid first name", {
										text : value
									});
						}
					}
				},
				middle_name : {
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Middle Name", {
										text : value
									});
						}
					}
				},
				last_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z']*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Last Name", {
										text : value
									});
						}
					}
				},
			
			
				category : {
					presence : true,
				},
				jee_main_roll_no : {
					presence : true,
					format : {
						pattern : /^[0-9]{6,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"Not a valid roll number", {
										text : value
									});
						}
					}
				},
				state : {
					presence : true,
				},

				
			
				phnoe_no : {
					presence : true,
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Mobile Number", {
										text : value
									});
						}
					}
				},
			   email : {
				// Email is 
					presence : true,
					// and must be an email (duh)
					email : true
				},
				date_of_birth : {
					presence : true,
					format : {
						pattern : /^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid DOB", {
										text : value
									});
						}
					}
				},
				
			};
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
<%@ include file="header.jsp" %>
 <!-- Left side column. contains the logo and sidebar -->
 <%@ include file="main-sidebar.jsp" %>

<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Student <small>Home</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#" class="active"><i class="fa fa-dashboard"></i>Home</a></li>

				</ol>
			</section>

			<!-- Modal -->


			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">
							<form id="csab">
								<input type="submit" />

								<table id="modal_table"
									class="table table-bordered table-striped">
									<tbody>

										<tr>
											<td>Registration Id</td>
											<td><input type=text class="form-control"
												id="registration_id" disabled></td>
										</tr>


										<tr>
											<td>Name</td>
											<td class="form-group"><input type="text" id="name"
												class="form-control" name="name" value="">
												<div class="messages"></div></td>
										</tr>
										<tr>
											<td>First Name</td>
											<td><div class="form-group">
													<input type="text" id="first_name" name="first_name"
														class="form-control" value="">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Middle Name</td>
											<td><div class="form-group">
													<input type="text" id="middle_name" name="middle_name"
														class="form-control" value="">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Last Name</td>
											<td><div class="form-group">
													<input type="text" id="last_name" name="last_name"
														class="form-control" value="">
													<div class="messages"></div>
												</div></td>
										</tr>

										<tr>
											<td>Category</td>
											<td><div class="form-group">
													<input type="text" id="category" name="category"
														class="form-control" value="">

													<!-- <select class="form-control" id="category"
												name="category">
													<option value="">Choose Category</option>
													<option value="general">General</option>
													<option value="obc">OBC</option>
													<option value="sc">SC</option>
													<option value="st">ST</option>
											</select> -->
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>JEE Main Roll No.</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														id="jee_main_roll_no" name="jee_main_roll_no">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>JEE Advanced Roll No.</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														id="jee_advanced_roll_no" name="jee_advance_roll_no">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>State</td>
											<td><div class="form-group">
													<input type="text" class="form-control" name="state"
														id="state">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Phone Number</td>
											<td><div class="form-group">
													<input type="text" class="form-control" name="phone_no"
														id="phone_no">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><div class="form-group">
													<input type="text" class="form-control" name="email"
														id="email">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Date Of Birth</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														name="date_of_birth" id="date_of_birth">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Program Allocated</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														name=program_allocated id="program_allocated">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Allocated Category</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														name="allocated_category" id="allocated_category">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Allocated Rank</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														name="allocated_rank" id="allocated_rank">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Status</td>
											<td><div class="form-group">
													<div class="form-group">
														<input type="text" class="form-control" name="status"
															id="status">
														<div class="messages"></div>
													</div></td>
										</tr>
										<tr>
											<td>Choice Number</td>
											<td><div class="form-group">
													<input type="text" class="form-control"
														name="choice_number" id="choice_number">
													<div class="messages"></div>
												</div></td>
										</tr>
										<tr>
											<td>Physically Disabled</td>
											<td>
												<div id="physically_disabled">
													<input type="radio" name="pwd" id="yes" value="Yes">Yes
													<input type="radio" name="pwd" id="no" value="No">No
												</div>
											</td>
										</tr>
										<tr>
											<td>Gender</td>
											<td><input class="form-control" class="form-control"
												type="text" name="gender" id="gender"></td>
										</tr>
										<tr>
											<td>Quota</td>
											<td><input type="text" name="quota" class="form-control"
												id="quota"></td>
										</tr>
										<tr>
											<td>Round</td>
											<td><input class="form-control" class="form-control"
												name="round" type="text" id="round"></td>
										</tr>
										<tr>
											<td>Willingness</td>
											<td><input type="text" class="form-control"
												name="willingness" id="willingness"></td>
										</tr>
										<tr>
											<td>Address</td>
											<td><input type="text" class="form-control"
												name="address" id="address"></td>
										</tr>
										<tr>
											<td>RC Name</td>
											<td><input type="text" class="form-control"
												name="rc_name" id="rc_name"></td>
										</tr>
										<tr>
											<td>Nationality</td>
											<td><input type="text" class="form-control"
												name="nationality" id="nationality"></td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" onclick="update()" class="btn btn-warning"
								data-dismiss="modal" id="update_data">Update</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>


			<!-- Main content -->
			<!-- Modal 2 begin-->
			<!-- Modal -->

			<div class="modal fade" id="myModalToAdd" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Student Details</h4>
						</div>
						<div class="modal-body">

							<table id="modal_table"
								class="table table-bordered table-striped">
								<tbody>

									<tr>
										<td>Name</td>
										<td><div class="form-group">
												<input type="text" id="insert_name" name="name"
													class="form-control" value="">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>First Name</td>
										<td><div class="form-group">
												<input type="text" id="insert_first_name" name="first_name"
													class="form-control" value="">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Middle Name</td>
										<td><div class="form-group">
												<input type="text" id="insert_middle_name"
													name="middle_name" class="form-control" value="">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Last Name</td>
										<td><div class="form-group">
												<input type="text" id="insert_last_name" name="last_name"
													class="form-control" value="">
												<div class="messages"></div>
											</div></td>
									</tr>

									<tr>
										<td>Category</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="category"
													id="insert_category">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>JEE Main Roll No.</td>
										<td><div class="form-group">
												<input type="text" class="form-control"
													name="jee_main_roll_no" id="insert_jee_main_roll_no">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>JEE Advanced Roll No.</td>
										<td><div class="form-group">
												<input type="text" class="form-control"
													name="jee_advanced_roll_no"
													id="insert_jee_advanced_roll_no">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>State</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="state"
													id="insert_state">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Phone Number</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="phone_no"
													id="insert_phone_no">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="email"
													id="insert_email">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Date Of Birth</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="date_of_birth"
													id="insert_date_of_birth">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Program Allocated</td>
										<td><div class="form-group">
												<input type="text" class="form-control"
													name="program_allocated" id="insert_program_allocated">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Allocated Category</td>
										<td><div class="form-group">
												<input type="text" class="form-control"
													name="allocated_category" id="insert_allocated_category">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Allocated Rank</td>
										<td><div class="form-group">
												<input type="text" class="form-control"
													name="allocated_rank" id="insert_allocated_rank">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Status</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="status"
													id="insert_status">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Choice Number</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="choice_number"
													id="insert_choice_number">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Physically Disabled</td>
										<td>
											<div id="insert_physically_disabled">
												<input type="radio" name="pwd" id="yes" value="Yes">Yes
												<input type="radio" name="pwd" id="no" value="No">No
											</div>
										</td>
									</tr>
									<tr>
										<td>Gender</td>
										<td><div class="form-group">
												<input class="form-control" class="form-control" type="text"
													name="gender" id="insert_gender">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Quota</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="quota"
													id="insert_quota">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Round</td>
										<td><div class="form-group">
												<input class="form-control" class="form-control" type="text"
													name="round" id="insert_round">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Willingness</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="willingness"
													id="insert_willingness">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Address</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="address"
													id="insert_address">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>RC Name</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="rc_name"
													id="insert_rc_name">
												<div class="messages"></div>
											</div></td>
									</tr>
									<tr>
										<td>Nationality</td>
										<td><div class="form-group">
												<input type="text" class="form-control" name="nationality"
													id="insert_nationality">
												<div class="messages"></div>
											</div></td>
									</tr>
								</tbody>
							</table>

						</div>
						<div class="modal-footer">
							<button type="button" onclick="add()" class="btn btn-danger"
								data-dismiss="modal" id="update_data">Add</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>
			</form>

			<!--  Modal 2 end -->


			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Student List</h3>
								<button type="button" class="btn btn-block btn-default"
									data-toggle="modal" data-target="#myModalToAdd">
									<i class="glyphicon glyphicon-eye-open"></i>
								</button>
							</div>
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>View</th>
											<th>Delete</th>
											<th>Name</th>
											<th>JEE Main Roll No.</th>
											<th>State</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Program Allocated</th>
											<th>Allocated Category</th>
											<th>Physically Disabled</th>
											<th>Entry Date</th>
											<th>Reported</th>
										</tr>
									</thead>
									<tbody>
										<%
											ArrayList<Student> csab_list = postgreSQLDatabase.registration.Query.getCsabStudentList();
											Iterator<Student> iterator = csab_list.iterator();
											while (iterator.hasNext()) {
												users.Student current = iterator.next();
										%>
										<tr>
											<td><button type="button"
													class="btn btn-block btn-default" data-toggle="modal"
													data-target="#myModal"
													onclick="displayProfile(<%=current.getCsab_id()%>)">
													<i class="glyphicon glyphicon-eye-open"></i>
												</button></td>
											<td><button type="button"
													class="btn btn-block btn-danger"
													onclick="deleteCSABStudent(<%=current.getCsab_id()%>)">
													<i class="glyphicon glyphicon-trash"></i>
												</button></td>

											<td><%=current.getName()%></td>
											<td><%=current.getJee_main_rollno()%></td>
											<td><%=current.getState_eligibility()%></td>
											<td><%=current.getMobile()%></td>
											<td><%=current.getEmail()%></td>
											<td><%=current.getProgram_allocated()%></td>
											<td><%=current.getAllocated_category()%></td>
											<td><%=current.isPwd()%></td>
											<td><%=current.getEntry_date()%></td>
											<td><%=current.isReported()%></td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</section>
			<!-- Main content -->

			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		 <%@ include file="footer.jsp" %>
  <!-- Control Sidebar -->
  <%@ include file="control-sidebar.jsp" %>
		
			</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
	<script>
		// Before using it we must add the parse and format functions
		// Here is a sample implementation using moment.js
		validate.extend(validate.validators.datetime, {
			// The value is guaranteed not to be null or undefined but otherwise it
			// could be anything.
			parse : function(value, options) {
				return +moment.utc(value);
			},
			// Input is a unix timestamp
			format : function(value, options) {
				var format = options.dateOnly ? "MM-DD-YYYY"
						: "YYYY-MM-DD hh:mm:ss";
				return moment.utc(value).format(format);
			}
		});

		// Hook up the form so we can prevent it from being posted
		var form = document.getElementById("csab");
		alert(form);
		form.addEventListener("submit", function(ev) {
			ev.preventDefault();
			handleFormSubmit(form);
		});
		// Hook up the inputs to validate on the fly
		var inputs = form.querySelectorAll("input, textarea, select")
		for (var i = 0; i < inputs.length; ++i) {
			inputs.item(i).addEventListener("change", function(ev) {
				var errors = validate(form, constraints) || {};

				showErrorsForInput(this, errors[this.name])
			});
		}

		function handleFormSubmit(form, input) {
			// validate the form aainst the constraints
			var errors = validate(form, constraints);
			// then we update the form to reflect the results

			if (!errors) {
				alert("success");
				form.submit();

			} else {
				showErrors(form, errors || {});
				alertify.error('Check the form for errors!');
			}
		}

		// Updates the inputs with the validation errors
		function showErrors(form, errors) {
			// We loop through all the inputs and show the errors for that input
			_
					.each(
							form
									.querySelectorAll("input[name], select[name], textarea[name]"),
							function(input) {
								// Since the errors can be null if no errors were found we need to handle
								// that
								showErrorsForInput(input, errors
										&& errors[input.name]);
							});
		}

		// Shows the errors for a specific input
		function showErrorsForInput(input, errors) {
			// This is the root of the input
			// Find where the error messages will be insert into
			var formGroup = closestParent(input.parentNode, "form-group")
			// Find where the error messages will be insert into
			, messages = formGroup.getElementsByClassName("messages")[0];
			// First we remove any old messages and resets the classes

			resetFormGroup(formGroup);
			// If we have errors
			if (errors) {
				// we first mark the group has having errors
				formGroup.classList.add("has-error");
				// then we append all the errors
				_.each(errors, function(error) {
					addError(messages, error);
				});
			} else {
				// otherwise we simply mark it as success
				formGroup.classList.add("has-success");
			}
		}

		// Recusively finds the closest parent that has the specified class
		function closestParent(child, className) {
			if (!child || child == document) {
				return null;
			}
			if (child.classList.contains(className)) {
				return child;
			} else {
				return closestParent(child.parentNode, className);
			}
		}

		function resetFormGroup(formGroup) {
			// Remove the success and error classes
			formGroup.classList.remove("has-error");
			formGroup.classList.remove("has-success");
			// and remove any old messages
			_.each(formGroup.querySelectorAll(".help-block.error"),
					function(el) {
						el.parentNode.removeChild(el);
					});
		}

		// Adds the specified error with the following markup
		// <p class="help-block error">[message]</p>
		function addError(messages, error) {
			var block = document.createElement("p");
			block.classList.add("help-block");
			block.classList.add("error");
			block.innerText = error;
			messages.appendChild(block);
		}

		function showSuccess() {
			// We made it \:D/
			alert("Success!");
		}
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="../plugins/morris/morris.min.js"></script>
	<!-- Sparkline -->
	<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="../plugins/knob/jquery.knob.js"></script>
	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="../plugins/daterangepicker/daterangepicker.js"></script>
	<!-- DataTables -->
	<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- datepicker -->
	<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script
		src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../dist/js/app.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

	<!-- AdminLTE for demo purposes -->
	<script src="../dist/js/demo.js"></script>
	<script>
  $(function () {
    $("#example1").DataTable({
		"paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
	});
  });
  
  
  function displayProfile(csab_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					var data=JSON.parse(xmlhttp.responseText);
					alert(xmlhttp.responseText);
					document.getElementById("name").value=data.name;
					document.getElementById("category").value=data.category;
					document.getElementById("jee_main_roll_no").value=data.jee_main_rollno;
					document.getElementById("jee_advanced_roll_no").value=data.jee_adv_rollno;
					document.getElementById("state").value=data.state;
					document.getElementById("phone_no").value=data.phone_number;
					document.getElementById("email").value=data.email;
					document.getElementById("date_of_birth").value=data.date_of_birth;
					document.getElementById("program_allocated").value=data.program_allocated;
					document.getElementById("allocated_category").value=data.allocated_category;
					document.getElementById("allocated_rank").value=data.allocated_rank;
					document.getElementById("status").value=data.status;
					document.getElementById("choice_number").value=data.choice_no;
					if(data.physically_disabled)
					document.getElementById("yes").checked=true;
					else
						document.getElementById("no").checked=true;
					
					document.getElementById("gender").value=data.gender
					document.getElementById("quota").value=data.quota;
					document.getElementById("round").value=data.round;
					document.getElementById("willingness").value=data.willingness;
					document.getElementById("address").value=data.permanent_address;
					document.getElementById("rc_name").value=data.rc_name;
					document.getElementById("nationality").value=data.nationality;
				//	document.getElementById("entry_date").value=data.entry_date;
					//document.getElementById("reported").value=data.reported;
					if(data.reported==false){
						document.getElementById("registration_id").value="Student has not yet reported";
					}
					else{
						document.getElementById("registration_id").value=data.registration_id;
					}
					document.getElementById("update_data").setAttribute("onclick","javascript:update("+csab_id+")");									
					
									
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../CsabStudentProfile",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("csab_id="+csab_id);
		}
		return false;

	}
  
  
  function update(csab_id){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();


		if(xmlhttp){
			var data= new FormData();
			data.append("csab_id",csab_id);
			data.append("name",document.getElementById("name").value);
			data.append("first_name",document.getElementById("first_name").value);
			data.append("middle_name",document.getElementById("middle_name").value);
			data.append("last_name",document.getElementById("last_name").value);
			data.append("category",document.getElementById("category").value);
			data.append("jee_main_rollno",document.getElementById("jee_main_roll_no").value);
			data.append("jee_advance_rollno",document.getElementById("jee_advanced_roll_no").value);
			data.append("state",document.getElementById("state").value);
			data.append("phone_number",document.getElementById("phone_no").value);
			data.append("email",document.getElementById("email").value);
			data.append("date_of_birth",document.getElementById("date_of_birth").value);
			data.append("program_allocated",document.getElementById("program_allocated").value);
			data.append("allocated_category",document.getElementById("allocated_category").value);
			data.append("allocated_rank",document.getElementById("allocated_rank").value);
			data.append("status",document.getElementById("status").value);
			data.append("choice_number",document.getElementById("choice_number").value);
			if(document.getElementById("yes").checked)
			data.append("pwd",document.getElementById("yes").value);
			else
				data.append("pwd",document.getElementById("no").value);
			
			data.append("gender",document.getElementById("gender").value);
			data.append("quota",document.getElementById("quota").value);
			data.append("round",document.getElementById("round").value);
			data.append("willingness",document.getElementById("willingness").value);
			data.append("address",document.getElementById("address").value);
			data.append("rc_name",document.getElementById("rc_name").value);
			data.append("nationality",document.getElementById("nationality").value);
			//document.getElementById("reported").value);
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../UpdateCSABStudentData?action=update",true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var outputLog = {}, iterator =data.entries(), end = false;
			while(end == false) {
			  var item = iterator.next();
			  if(item.value!=undefined) {
			      outputLog[item.value[0]] = item.value[1];
			  } else if(item.done==true) {
			      end = true;
			  }
			   }
			console.log(outputLog);
			                              
			                xmlhttp.send(data);
			            }
		return false;

	}

function add(){
		var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();


		if(xmlhttp){
			var data= new FormData();
			data.append("name",document.getElementById("insert_name").value);
			data.append("first_name",document.getElementById("insert_first_name").value);
			data.append("middle_name",document.getElementById("insert_middle_name").value);
			data.append("last_name",document.getElementById("insert_last_name").value);
			data.append("category",document.getElementById("insert_category").value);
			data.append("jee_main_rollno",document.getElementById("insert_jee_main_roll_no").value);
			data.append("jee_advance_rollno",document.getElementById("insert_jee_advanced_roll_no").value);
			data.append("state",document.getElementById("insert_state").value);
			data.append("phone_number",document.getElementById("insert_phone_no").value);
			data.append("email",document.getElementById("insert_email").value);
			data.append("date_of_birth",document.getElementById("insert_date_of_birth").value);
			data.append("program_allocated",document.getElementById("insert_program_allocated").value);
			data.append("allocated_category",document.getElementById("insert_allocated_category").value);
			data.append("allocated_rank",document.getElementById("insert_allocated_rank").value);
			data.append("status",document.getElementById("insert_status").value);
			data.append("choice_number",document.getElementById("insert_choice_number").value);
			if(data.physically_disabled==true)
			data.append("pwd",document.getElementById("insert_physically_disabled").value);
			else
				data.append("pwd",document.getElementById("insert_physically_disabled").value);
			data.append("gender",document.getElementById("insert_gender").value);
			data.append("quota",document.getElementById("insert_quota").value);
			data.append("round",document.getElementById("insert_round").value);
			data.append("willingness",document.getElementById("insert_willingness").value);
			data.append("address",document.getElementById("insert_address").value);
			data.append("rc_name",document.getElementById("insert_rc_name").value);
			data.append("nationality",document.getElementById("insert_nationality").value);
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					alert(xmlhttp.responseText);
				}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../UpdateCSABStudentData?action=add",true);
			//xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var outputLog = {}, iterator =data.entries(), end = false;
			while(end == false) {
			  var item = iterator.next();
			  if(item.value!=undefined) {
			      outputLog[item.value[0]] = item.value[1];
			  } else if(item.done==true) {
			      end = true;
			  }
			   }
			console.log(outputLog);
			                              
			                xmlhttp.send(data);
			            }
		return false;

	}

  
  
  function deleteCSABStudent(csab_id){
	  var xmlhttp;
		try{
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					//Browser doesn't support ajax	
					alert("Your browser is unsupported");
				}
			}
		}	
		//var xmlhttp=new XMLHttpRequest();

		if(xmlhttp){	
			xmlhttp.onreadystatechange=function() {
				if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					window.location.reload();
					//alert(xmlhttp.responseText);
							}
				if(xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			xmlhttp.open("POST","../DeleteCSABStudent",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("csab_id="+csab_id);
		}
		return false;
	  
  }

</script>
</body>
</html>
