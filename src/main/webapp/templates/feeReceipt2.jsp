
<%@page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="settings.database.PostgreSQLConnection"%>
<%@page import="utilities.StringFormatter"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="users.Student"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.feePayment.Payment"%>
<%@page import="postgreSQLDatabase.feePayment.Transaction"%>
<%@page import="org.json.JSONObject"%>
<html>
<head>
<style>
@page {
	size: A4;
}

.p2 {
	margin-left: 7%;
	margin-top: 3%;
}

.p1 {
	margin-left: 15%;
}

table {
	border: none;
	border-collapse: collapse;
}

th, td {
	border: 1px solid black;
}
</style>
</head>
<body>
	<title>Fee payment</title>

	<p align="center">
		<img alt=""
			src="../image/IIITKLetterHead.png"
			style="height: 99px; width: 729px" />
	</p>




	<%
		long ref_no = Long.parseLong(request.getParameter("ref_no"));

		Payment payment = postgreSQLDatabase.feePayment.Query.getFeePaymentByRefNo(ref_no);
		Transaction transaction = postgreSQLDatabase.feePayment.Query
				.getFeeTransactionById(payment.getTransaction_id());
		Student student = Query.getRegistrationStudentData(transaction.getReg_id());
	%>
 <center><strong><u>FEE PAYMENT ACKNOWLEDGEMENT</u></strong></center>

	<p class="p2">
		<strong>Dear <%=student.getName()%>, your payment details are as follows
		</strong>
	</p>



	<p class="p2">
	<table style="width: 50%;" align="center">
		<tr>
			<td style='font-weight: bold !important;'>Name:</td>
			<td><%=student.getName()%></td>
		</tr>
	<%if (student.getStudent_id()!=null){ %>
		<tr>
			<td style='font-weight: bold !important;'>Student ID:</td>
			<td><%=student.getStudent_id()%></td>
		</tr>
		<%} %>

		<tr>
			<td style='font-weight: bold !important;'>Registration ID:</td>
			<td><%=student.getRegistration_id()%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Semester:</td>
			<td><%=student.getSemester()%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Category:</td>
			<td><%=student.getCategory()%></td>
		</tr>

	</table>
	<br />
	</p>





	<p class="p2">
		<strong>Transaction Details</strong>
	<table style="width: 50%;" align="center">
		<tr>
			<td style='font-weight: bold !important;'>Transaction Id:</td>
			<td><%=transaction.getTransaction_id()%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Semester:</td>
			<td><%=transaction.getSemester()%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Category:</td>
			<td><%=transaction.getCategory()%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Date:</td>
			<td><% SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			
			out.println(df.format(transaction.getDate()));%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Completed:</td>
			<td><%=StringFormatter.booleanToString(transaction.getCompleted())%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Reference no.</td>


			<td><%=ref_no%></td>



		</tr>

	</table>
	</p>

	<p class="p2">
		<strong>Payment Details</strong>
	<table style="width: 50%;" align="center">
		<tr>
			<td style='font-weight: bold !important;'>Payment Method</td>
			<td><%=payment.getPayment_method()%></td>
		</tr>
		<tr>
			<td style='font-weight: bold !important;'>Comment:</td>
			<td><%=payment.getComment()%></td>
		</tr>

		<tr>
			<td style='font-weight: bold !important;'>Amount:</td>
			<td><%=payment.getAmount()%></td>
		</tr>


		<tr>
			<td style='font-weight: bold !important;'>Details:</td>


			<td>
				<table>
					<%
						JSONObject details = payment.getDetails();
						Iterator<String> iterator2 = details.keys();
						while (iterator2.hasNext()) {
							String key = iterator2.next();
							out.print("<tr style='border:none !important'><td style='border:none !important'><strong>"
									+ key.toUpperCase() + ":</strong></td><td style='border:none !important'>" + details.get(key)
									+ "</td></tr>");
						}
					%>
				</table>
			</td>

		</tr>

	</table>
	<p>Please Note: This is a computer generated receipt for representational purposes only. Contact the office of IIIT Kota for any conflicts/clarifications.</p>

</body>
</html>