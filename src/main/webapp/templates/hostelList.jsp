<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="users.Student"%>
    <%@page import="java.util.*"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
@page{
size:A4;
}
table, tr, td, th {border:1px solid black; border-collapse:collapse;}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hostellers List</title>
</head>
<body>

							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>S.No.</th>
											<th>Student ID</th>
											<th>Name</th>
											<th>Category</th>
											<th>Address</th>
											<th>Phone Number</th>
											<th>Email</th>
											<th>Date Of Birth</th>
											
											<th>Physically Disabled</th>
											<th>Gender</th>
											<%if(request.getParameter("hosteller")==null){%><th>Hosteller</th><%} %>
										</tr>
									</thead>
									<tbody>
										<%int index=1;
											ArrayList<Student> registration_list = Query.getStudentRegistrationList();
											Iterator<Student> iterator = registration_list.iterator();
											while (iterator.hasNext()) {
												Student current = iterator.next();
												if(!current.isHosteller())continue;
												if(request.getParameter("hosteller")!=null&&((request.getParameter("hosteller").equals("true")&&!current.isHosteller())||(request.getParameter("hosteller").equals("false")&&current.isHosteller())))continue;
										%>
										<tr>
										    <td><center><%=index++%></center></td>
											<td><center><%=current.getStudent_id()%></center></td>
											<td><center><%=current.getName()%></center></td>
											<td><center><%=current.getCategory()%></center></td>
                                            <td><center><%=current.getPermanent_address()%></center></td>
								            <td><center><%=current.getMobile()%></center></td>
											<td><center><%=current.getEmail()%></center></td>
											<td><center><%=current.getDate_of_birth()%></center></td>
										    
											<td><center><%=current.isPwd()%></center></td>
											<td><center><%=current.getGender()%></center></td>
										<%if (request.getParameter("hosteller")==null){%><td><center><%=current.isHosteller()%></center></td>
										<%} %>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						
</body>
<script type="text/javascript">
//window.print();
</script>
</html>