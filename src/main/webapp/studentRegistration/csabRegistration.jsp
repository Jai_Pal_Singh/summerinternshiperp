	<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ERP IIITK</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.9.0/validate.min.js"></script>
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/alertify.min.css">
<link rel="stylesheet"
	href="../plugins/alertify/alertifyjs/css/themes/default.min.css">
<script src="../plugins/alertify/alertifyjs/alertify.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="users.Student"%>
<script>
	var constraints = [

			{},

			{

				email : {
					// Email is 
					presence : true,
					// and must be an email (duh)
					email : true
				},
				first_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid first name", {
										text : value
									});
						}
					}
				},
				middle_name : {
					format : {
						pattern : /^[a-zA-Z]{2,}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Middle Name", {
										text : value
									});
						}
					}
				},
				last_name : {
					
					format : {
						pattern : /^[a-zA-Z']*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Last Name", {
										text : value
									});
						}
					}
				}
			},
			{
				father_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z' \.]*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Name", {
										text : value
									});
						}
					}
				},
				mother_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z' \. ]*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a Surname Name", {
										text : value
									});
						}
					}
				},
				father_contact : {
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Mobile Number", {
										text : value
									});
						}
					}
				},

				mother_contact : {
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid mobile number", {
										text : value
									});

						}
					}
				}
			},
			{
				permanent_address : {
					presence : true
				},
				local_address : {
					presence : true
				},
				hosteller : {
					presence : true
				},
				hostel : {

				},
				hostel_room : {

				}
			},
			{
				guardian_name : {
					presence : true,
					format : {
						pattern : /^[a-zA-Z' ]*$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a Surname Name", {
										text : value
									});
						}
					}
				},
				guardian_contact : {
					presence : true,
					format : {
						pattern : /^\+?[0-9]{10,12}$/,
						message : function(value, attribute, validatorOptions,
								attributes, globalOptions) {
							return validate.format(
									"^%{text} is not a valid Mobile Number", {
										text : value
									});
						}
					}
				},
				guardian_email : {
				
					// and must be an email (duh)
					email : true,
					presence:true
				},
				guardian_address : {
					presence:true
				},
			}

	];
</script>
<script type="text/javascript">
	function navigate() {

		init(step);
		if (step == 1) {
			$("#step-1").toggle("slide", "right", 500);
			$("#step-2").hide();
			$("#step-3").hide();
			$("#step-4").hide();
		}
		if (step == 2) {
			$("#step-2").toggle("slide", {
				direction : 'right'
			}, 500);
			$("#step-1").hide();
			$("#step-3").hide();
			$("#step-4").hide();

		}
		if (step == 3) {
			$("#step-3").toggle("slide", {
				direction : 'right'
			}, 500);
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-4").hide();

		}
		if (step == 4) {
			$("#step-4").toggle("slide", {
				direction : 'right'
			}, 500);
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-3").hide();
		}
	}

	$(document).ready(function() {
		$("#step-2").hide();
		$("#step-3").hide();
		$("#step-4").hide();
		$("#prev_2").click(function() {
			step = 1;
			init(step);
			$("#step-1").toggle("slide", {
				direction : 'left'
			}, 500);
			$("#step-2").hide();
			$("#step-3").hide();
			$("#step-4").hide();
		});
		$("#prev_3").click(function() {
			step = 2;
			init(step);
			$("#step-2").toggle("slide", {
				direction : 'left'
			}, 500);
			$("#step-1").hide();
			$("#step-3").hide();
			$("#step-4").hide();
		});
		$("#prev_4").click(function() {
			step = 3;
			init(step);
			$("#step-3").toggle("slide", {
				direction : 'left'
			}, 500);
			$("#step-1").hide();
			$("#step-2").hide();
			$("#step-4").hide();
		});

	});
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini"
	style="background: url('../image/image.jpg'); background-size: cover;">
	<div class="wrapper">
		<%@ include file="header.jsp"%>


		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar col-xs-6">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<br>
				
			</section>
			<!-- /.sidebar -->
		</aside>

		<div class="content-wrapper" style="min-height: 901px;">
			<!-- Content Header (Page header) -->
			<section class="content-header"></section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3"></div>

						<div class="col-md-6">
							<%
								System.out.println("session=" + request.getSession().getAttribute("reg_id"));
								long reg_id = Long.parseLong(request.getSession().getAttribute("reg_id").toString());

								Student current = Query.getRegistrationStudentData(reg_id);
							%>
							<div id="step-1">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Update your profile</h4>
										<h3>Step 1</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post" id="form1">

												<div class="form-group">
													<label>Name</label> <input type="text" class="form-control"
														id="name" placeholder="Student Name"
														value="<%=current.getName()%>" disabled>
													<div class="messages"></div>

												</div>
												<div class="form-group">
													<label>First Name</label> <input type="text"
														name="first_name" class="form-control" id="first_name"
														placeholder="First Name"
														value="<%=current.getFirst_name()%>">
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Middle Name</label> <input type="text"
														name="middle_name" class="form-control" id="middle_name"
														placeholder="Middle Name"
														value="<%=current.getMiddle_name()%>">
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Last Name</label> <input type="text"
														name="last_name" class="form-control" id="last_name"
														placeholder="Last Name"
														value="<%=current.getLast_name()%>">
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Mobile Number</label> <input type="text"
														name="mobile" class="form-control" id="mobile"
														placeholder="Phone Number"
														value="<%=current.getMobile()%>">
													<div class="messages"></div>
												</div>


												<div class="form-group">
													<label>Date Of Birth(mm/dd/yyyy)</label> <input type="date"
														class="form-control" placeholder="Date of Birth"
														id="date_of_birth" value="<%SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
													out.print(df.format(current.getDate_of_birth()));%>"
														disabled>
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>State Eligibility</label> <input type="text"
														class="form-control" placeholder="State"
														id="state_eligibility"
														value="<%=current.getState_eligibility()%>" disabled>
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Email</label> <input type="text" name="email"
														class="form-control" placeholder="Email " id="email" />
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Category</label> <input type="text"
														class="form-control" placeholder="category " id="category"
														value="<%=current.getCategory()%>" disabled>
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Gender</label> <input type="text"
														class="form-control" placeholder="gender " id="gender"
														value="<%=current.getGender()%>" disabled>
													<div class="messages"></div>
												</div>


												<div class="row">
													<div class="col-xs-4 pull-right">
														<input type="submit" id="step1" value="Next"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>
								</div>
							</div>

							<div id="step-2">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Update your profile</h4>
										<h3>Step 2</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form id="form2">




												<div class="form-group">
													<label>Father's Name</label> <input type="text"
														name="father_name" id="father_name" class="form-control"
														placeholder="Father's Name"
														value="<%=current.getFather_name()%>">
													<div class="messages"></div>
												</div>
												<div class="form-group">
													<label>Mother's Name</label> <input type="text"
														name="mother_name" id="mother_name" class="form-control"
														placeholder="Mother's Name"
														value="<%=current.getMother_name()%>">
													<div class="messages"></div>
												</div>
												<div class="form-group">
													<label>Father's Contact</label> <input type="text"
														name="father_contact" id="father_contact"
														class="form-control" placeholder="Father's Contact">
													<div class="messages"></div>
												</div>
												<div class="form-group">
													<label>Mother's Contact Number</label> <input type="text"
														name="mother_contact" id="mother_contact"
														class="form-control" placeholder="Mother's Contact">
													<div class="messages"></div>
												</div>


												<div class="form-group">
													<label>Program Allocated</label> <input type="text"
														id="program_allocated" class="form-control"
														placeholder="Program Allocated"
														value="<%=current.getProgram_allocated()%>" disabled>

												</div>


												<div class="row">
													<div class="col-xs-4">
														<input type="button" id="prev_2" value="Previous"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="submit" id="step2" value="Next"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

							<div id="step-3">

								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Update your profile</h4>
										<h3>Step 3</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post" id="form3">
												<div class="form-group">
													<label>Permanent Address</label>
													<textarea name="permanent_address" id="permanent_address" onchange="address_copy()"
														class="form-control" placeholder="Permanent Address"></textarea>
													<div class="messages"></div>

												</div>

												<div class="form-group">
													<label>I require a Hostel</label> <select class="form-control"
														name="hosteller" id="hosteller">
														<option value="yes">Yes</option>
														<option value="no">No</option>
													</select>
													<div class="messages"></div>
												</div>
												<div class="form-group">
												<label>Local Address Same as Permanent Address</label>
												<input type="checkbox" id="address_copier"  onchange="address_copy()"/>
													<label>Local Address</label>
													<textarea name="local_address" id="local_address"
														class="form-control" placeholder="Local Address" onchange="address_copy()"></textarea>
													<div class="messages"></div>
												</div>
												<div class="form-group">
													<label>Hostel Name (if applicable)</label>
													<textarea name="hostel" id="hostel" class="form-control"
														placeholder="Hostel Name/ Address"></textarea>
													<div class="messages"></div>
												</div>

												<div class="form-group">
													<label>Hostel Room (if applicable)</label> <input type="text"
														name="hostel_room" id="hostel_room" 
														placeholder="Hostel Room No.">
													<div class="messages"></div>
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" id="prev_3" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="submit" id="step3" value="Next"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

							<div id="step-4">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Update your profile</h4>
										<h3>Step 4</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post" id="form4">
												<div class="form-group">
													<label>Guardian's Name</label> <input type="text"
														name="guardian_name" id="guardian_name"
														class="form-control" placeholder="Guardian Name">
													<div class="messages"></div>
												</div>
												<div class="form-group">
													<label>Guardian's Contact Number</label> <input type="text"
														name="guardian_contact" id="guardian_contact"
														class="form-control" placeholder="Guardian Contact">
													<div class="messages"></div>
												</div>
												<div class="form-group">
													<label>Guardian's Email</label> <input type="text"
														name="guardian_email" id="guardian_email"
														class="form-control" placeholder="Guardian Email">
													<div class="messages"></div>
												</div>


												<div class="form-group">
													<label>Guardian's Address</label>
													<textarea name="guardian_address" id="guardian_address"
														class="form-control" placeholder="Guardian Address"></textarea>
													<div class="messages"></div>
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" id="prev_4" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
														<div class="messages"></div>
													</div>
													<div class="col-xs-4 pull-right">
														<input type="submit" id="step4" value="Submit"
															class="btn btn-primary btn-block btn-flat"
															/>
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

						</div>
						<div class="col-md-3"></div>

					</div>
				</div>
			</section>
			<!-- /.content -->
		</div>
	</div>
	<script>
		// Before using it we must add the parse and format functions
		// Here is a sample implementation using moment.js
		validate.extend(validate.validators.datetime, {
			// The value is guaranteed not to be null or undefined but otherwise it
			// could be anything.
			parse : function(value, options) {
				return +moment.utc(value);
			},
			// Input is a unix timestamp
			format : function(value, options) {
				var format = options.dateOnly ? "MM-DD-YYYY"
						: "YYYY-MM-DD hh:mm:ss";
				return moment.utc(value).format(format);
			}
		});

		// Hook up the form so we can prevent it from being posted
		var form;
		var inputs;
		var step = 1;
	
		function init(step) {
			form = document.getElementById("form" + step);
			if(form==null)return;
			form.addEventListener("submit", function(ev) {
				ev.preventDefault();
				handleFormSubmit(form);
			});
			inputs = form.querySelectorAll("input, textarea, select")
			for (var i = 0; i < inputs.length; ++i) {
				inputs.item(i).addEventListener("change", function(ev) {
					var errors = validate(form, constraints[step]) || {};

					showErrorsForInput(this, errors[this.name])
				});
			}
		}

		// Hook up the inputs to validate on the fly

		function handleFormSubmit(form) {
			
			// validate the form aainst the constraints
			var errors = validate(form, constraints[step]);
			// then we update the form to reflect the results
 
			if (!errors) {
				//	alertify.success('You changes have been saved!');
				//	form.submit();
				
			if(step!=4){
				step = step + 1;
				navigate();
			}
			else{updateStudentRegistrationDetails();
			

			
			}
				
                 
			} else {
				showErrors(form, errors || {});
				alertify.error('Check the form for errors!');
			}
		}

		// Updates the inputs with the validation errors
		function showErrors(form, errors) {
			// We loop through all the inputs and show the errors for that input
			_
					.each(
							form
									.querySelectorAll("input[name], select[name], textarea[name]"),
							function(input) {
								// Since the errors can be null if no errors were found we need to handle
								// that
								showErrorsForInput(input, errors
										&& errors[input.name]);
							});
		}

		// Shows the errors for a specific input
		function showErrorsForInput(input, errors) {
			// This is the root of the input
			// Find where the error messages will be insert into
			var formGroup = closestParent(input.parentNode, "form-group")
			// Find where the error messages will be insert into
			, messages = formGroup.getElementsByClassName("messages")[0];
			// First we remove any old messages and resets the classes

			resetFormGroup(formGroup);
			// If we have errors
			if (errors) {
				// we first mark the group has having errors
				formGroup.classList.add("has-error");
				// then we append all the errors
				_.each(errors, function(error) {
					addError(messages, error);
				});
			} else {
				// otherwise we simply mark it as success
				formGroup.classList.add("has-success");
			}
		}

		// Recusively finds the closest parent that has the specified class
		function closestParent(child, className) {
			if (!child || child == document) {
				return null;
			}
			if (child.classList.contains(className)) {
				return child;
			} else {
				return closestParent(child.parentNode, className);
			}
		}

		function resetFormGroup(formGroup) {
			// Remove the success and error classes
			formGroup.classList.remove("has-error");
			formGroup.classList.remove("has-success");
			// and remove any old messages
			_.each(formGroup.querySelectorAll(".help-block.error"),
					function(el) {
						el.parentNode.removeChild(el);
					});
		}

		// Adds the specified error with the following markup
		// <p class="help-block error">[message]</p>
		function addError(messages, error) {
			var block = document.createElement("p");
			block.classList.add("help-block");
			block.classList.add("error");
			block.innerText = error;
			messages.appendChild(block);
		}

		function showSuccess() {
			// We made it \:D/
			alert("Success!");
		}
		init(step);
	</script>

	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../plugins/knob/jquery.knob.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="../plugins/daterangepicker/daterangepicker.js"></script>
	<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<script src="../dist/js/app.min.js"></script>
	<script>
		function updateStudentRegistrationDetails() {

			

			//var chat_name=document.getElementById('chat_message').value;

			var xmlhttp;
			try {
				xmlhttp = new XMLHttpRequest();
			} catch (e) {
				// Internet Explorer Browsers
				try {
					xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {
						//Browser doesn't support ajax	
						alert("Your browser is unsupported");
					}
				}
			}
			//var xmlhttp=new XMLHttpRequest();

			if (xmlhttp) {
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						alertify.confirm("Data Updation Success!","Your data has been updated! Please go to registration desk for verification.",function(){window.location.href="../SignOut?registration=true";},function(){window.location.href="../SignOut?registration=true";});
						

					}
					if (xmlhttp.status == 404)
						alert("Could not connect to server");
					
				}
				xmlhttp.open("POST", "../addUpdateStudentRegistrationDetails",
						true);
				xmlhttp.setRequestHeader("Content-type",
						"application/x-www-form-urlencoded");
				console.log("first_name="
						+ document.getElementById('first_name').value
						+ "&middle_name="
						+ document.getElementById('middle_name').value
						+ "&last_name="
						+ document.getElementById('last_name').value
						//+"&category="+document.getElementById('category').value
						+ "&guardian_name="
						+ document.getElementById('guardian_name').value
						+ "&guardian_contact="
						+ document.getElementById('guardian_contact').value
						+ "&guardian_email="
						+ document.getElementById('guardian_email').value
						+ "&guardian_address="
						+ document.getElementById('guardian_address').value
						+ "&father_name="
						+ document.getElementById('father_name').value
						+ "&mother_name="
						+ document.getElementById('mother_name').value
						+ "&father_contact="
						+ document.getElementById('father_contact').value
						+ "&mother_contact="
						+ document.getElementById('mother_contact').value
						//+"&gender="+document.getElementById('gender').value
						//+"&date_of_birth="+document.getElementById('date_of_birth').value
						//+"&state_eligibility="+document.getElementById('state_eligibility').value
						//+"&program_allocated="+document.getElementById('program_allocated').value
						+ "&mobile="
						+ document.getElementById('mobile').value
						+ "&email="
						+ document.getElementById('email').value
						+ "&permanent_address="
						+ document.getElementById('permanent_address').value
						+ "&local_address="
						+ document.getElementById('local_address').value
						+ "&hosteller="
						+ document.getElementById('hosteller').value
						+ "&hostel="
						+ document.getElementById('hostel').value
						+ "&room="
						+ document.getElementById('hostel_room').value);
				
				xmlhttp
						.send(//"name="+document.getElementById('name').value
						"first_name="
								+ document.getElementById('first_name').value
								+ "&middle_name="
								+ document.getElementById('middle_name').value
								+ "&last_name="
								+ document.getElementById('last_name').value
								//+"&category="+document.getElementById('category').value
								+ "&guardian_name="
								+ document.getElementById('guardian_name').value
								+ "&guardian_contact="
								+ document.getElementById('guardian_contact').value
								+ "&guardian_email="
								+ document.getElementById('guardian_email').value
								+ "&guardian_address="
								+ document.getElementById('guardian_address').value
								+ "&father_name="
								+ document.getElementById('father_name').value
								+ "&mother_name="
								+ document.getElementById('mother_name').value
								+ "&father_contact="
								+ document.getElementById('father_contact').value
								+ "&mother_contact="
								+ document.getElementById('mother_contact').value
								//+"&gender="+document.getElementById('gender').value
								//+"&date_of_birth="+document.getElementById('date_of_birth').value
								//+"&state_eligibility="+document.getElementById('state_eligibility').value
								//+"&program_allocated="+document.getElementById('program_allocated').value
								+ "&mobile="
								+ document.getElementById('mobile').value
								+ "&email="
								+ document.getElementById('email').value
								+ "&permanent_address="
								+ document.getElementById('permanent_address').value
								+ "&local_address="
								+ document.getElementById('local_address').value
								+ "&hosteller="
								+ document.getElementById('hosteller').value
								+ "&hostel="
								+ document.getElementById('hostel').value
								+ "&room="
								+ document.getElementById('hostel_room').value);

			}
			return false;
		}
		
		function address_copy(){
			var copy=document.getElementById("address_copier");
			if(copy.checked){
					document.getElementById("local_address").value=document.getElementById("permanent_address").value;
			document.getElementById("local_address").disabled=true;
		}else{ document.getElementById("local_address").disabled=false;}
		}
		
	</script>
</body>

</html>