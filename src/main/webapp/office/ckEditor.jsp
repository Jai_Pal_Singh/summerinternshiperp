<%@page import="settings.propertySheet.PropertiesFile"%>
<%@page import="postgreSQLDatabase.templates.Template"%>
<%@page import="postgreSQLDatabase.templates.Query"%>
<%@page import="ftp.FTPUpload"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CKEDITOR</title>
<style>
body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  overflow:hidden;
}
page[size="A4"][layout="portrait"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="landscape"]{
  width: 29.7cm;
  height: 21cm;  
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}
</style>

</head>
<body>
<section class="content-header">

<fieldset style="background-color:#337ab7;border:none;">
<%if(request.getParameter("template_id")!=null){ 
long template_id=Long.parseLong(request.getParameter("template_id"));
	 Template template= Query.getTemplates(template_id);
	 String title=template.getTitle();
	   String file=FTPUpload.readTextFile(template.getFilePath());
	 
     

%>
	<button onclick="update(<%=template_id%>)">Update</button>
	<input type="text" value="<%=title %>" id="title" placeholder="Template title" readonly/>
	<% }else {%>
	<button onclick="save()">Save</button>
	<input type="text" value="" id="title" placeholder="Template title"/>
	<%} %>
</fieldset>
			</section>


   <page size="A4" layout="portrait">
   <div id="editor1" contenteditable="true" style="margin: 1cm 1cm 2cm 1cm;" >
   <%if(request.getParameter("template_id")!=null){
	   long template_id=Long.parseLong(request.getParameter("template_id"));
	 Template template= Query.getTemplates(template_id);
	   String file=FTPUpload.readTextFile(template.getFilePath());
	   out.print(file);
   }
   else{
%>
<p><img alt="" src="http://<%=PropertiesFile.getFTPServer()%>/ftp/templates/IIITKLetterHead.png" style="height:99px; width:729px" /></p>
<br/>
 
	 <%} %>
   </div> 
   </page>

<input type ="hidden" value="heya" id="test"/>
</body>
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="../plugins/ckeditor/ckeditor.js"></script>
	
<script>

CKEDITOR.disableAutoInline = true;
CKEDITOR.inline( 'editor1',{extraPlugins: 'tags,sourcedialog'});
function save() {
	var file = CKEDITOR.instances.editor1.getData();
	var name=document.getElementById("title").value;
	if(name==""){
			alert("Please set a title");
		return;
	}
	
	if(file==""){
			alert("Cannot save empty template");
				return;
	}

	var xmlhttp;
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer Browsers
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				//Browser doesn't support ajax	
				alert("Your browser is unsupported");
			}
		}
	}
	
	if (xmlhttp) {
		var data= new FormData();
		data.append("title",name);
		data.append("file",file);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
window.location.href="templatesList.jsp";
			}
			if (xmlhttp.status == 404)
				alert("Could not connect to server");
		}
		xmlhttp.open("POST", "../SaveTemplates",true);
		xmlhttp	.send(data);
	}
	
}



function update(template_id) {
	var file = CKEDITOR.instances.editor1.getData();

	if(file==""){
			alert("Cannot save empty template");
				return;
	}

	var xmlhttp;
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer Browsers
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				//Browser doesn't support ajax	
				alert("Your browser is unsupported");
			}
		}
	}
	
	if (xmlhttp) {
		var data= new FormData();
		data.append("template_id",template_id);
		data.append("file",file);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
window.location.href="templatesList.jsp";
			}
			if (xmlhttp.status == 404)
				alert("Could not connect to server");
		}
		xmlhttp.open("POST", "../UpdateTemplates",true);
		xmlhttp	.send(data);
	}
	
}






















</script>
</html>