<!DOCTYPE html>
<%@page
	import="postgreSQLDatabase.feePayment.RegistrationFeePaymentQuery"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONArray"%>
<%@page import="postgreSQLDatabase.registration.Query"%>

<%@page import="org.json.JSONObject"%>


<%@ page import="users.Student"%>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ERP IIITK</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="../plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="../plugins/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<link rel="stylesheet"
	href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link rel="stylesheet"
	href="../plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet"
	href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<script type="text/javascript">
	var a;
	$(document).ready(function() {
	
		$("#step-3").hide();
		$("#step-4").hide();
		$("#step-5").hide();
		$("#step-6").hide();
		$("#step-7").hide();
		$("#error").hide();

		

	

	});

	function showPayment() {
		a = $('input[name=payment]:checked').val();
		if (a == null) {
			$("#error").show();
		} else {
			$("#step-2").hide();
			$("#" + a).toggle('slide', 'right', 500);
		}
	}

	function back() {
		$("#" + a).hide();
		$("#step-2").toggle('slide', 'right', 500);
	}
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini"
	style="background: url('../image/image.jpg'); background-size: cover;">
	<input type="hidden" id="transaction_id"
		value="<%=request.getParameter("transaction_id")%>" />
	<div class="wrapper">
		<%@ include file="header.jsp"%>

		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="main-sidebar.jsp"%>

		<div class="content-wrapper" style="min-height: 901px;">
			<!-- Content Header (Page header) -->
			<section class="content-header"></section>

			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div id="step-2">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Refund Payment</h4>
										<h3>Step 1</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post" id="payment_method">
												<div class="form-group">
													<label for="exampleInputPassword1">Registration ID
														: </label>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Student Name: </label>
												</div>
												<span id="error">
													<div class="form-group">
														<label style="color: red;">Please select any one
															of the payment method</label>
													</div>
												</span>
												<div class="form-group">


													<div class="radio">
														<label> <input type="radio" name="payment"
															onChange="$('#error').html('')" id="optionsRadios1"
															value="step-3"> Demand Draft
														</label>
													</div>
													<div class="radio">
														<label> <input type="radio" name="payment"
															onChange="$('#error').html('')" id="optionsRadios1"
															value="step-4"> Challan
														</label>
													</div>
													<div class="radio">
														<label> <input type="radio" name="payment"
															onChange="$('#error').html('')" id="optionsRadios1"
															value="step-7"> NEFT
														</label>
													</div>

												</div>

												<div class="row">

													<div class="col-xs-4 pull-center">
														<input type="button" name="step-2" id="step2"
															onclick="showPayment()" value="Next"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

							<div id="step-3">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Payment</h4>
										<h3>Step 2</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>Demand Draft</h3>
												</div>

												<div class="form-group has-feedback">
													<label>DD Number</label> <input type="text" name="dd"
														id="dd_no" class="form-control" placeholder="DD Number">
												</div>

												<div class="form-group has-feedback">
													<label>Choose Bank</label> <select class="form-control"
														name="bank_list" id="dd_bank">
														<option value="">----Choose Bank----</option>
														<option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option><option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option>
<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>
<option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option>
<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>
<option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option>
<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>

<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>

													</select>
												</div>



												<div class="form-group has-feedback">
													<label for="exampleInputPassword1">Payable To :
														Director, IIIT Kota</label>
												</div>

												<div class="form-group has-feedback">
													<label for="exampleInputPassword1">Payable At :
														Jaipur</label>
												</div>

												<div class="form-group">
													<div class="input-group">
														<label>Date Of DD</label> <input type="text"
															name="dateofDD" id="dd_date" class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>
													<!-- /.input group -->
												</div>

												<div class="form-group has-feedback">
													<label>DD Amount</label> <input type="text" id="dd_amount"
														name="amountDD" class="form-control" placeholder="Amount">
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" onclick="makePayment('dd')"
															name="submitDD" value="Submit"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>

							<div id="step-4">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Refund payment-Challan</h4>
										<h3>Step 2</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>Challan</h3>
												</div>


												

												<div class="form-group">
													<div class="input-group">
														<label>Challan Amount</label> <input type="text" value=""
															id="challan_amount" placeholder="Challan Amount"
															class="form-control">
													</div>
												</div>
												<div class="form-group">
													<div class="input-group">
														<label>Challan Date</label> <input type="text"
															name="dateofChallan" id="challan_date"
															class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>
												</div>
												<div class="form-group">
													<div class="input-group">
														<label>Bank</label> <select class="form-control"
															name="bank" id="challan_bank">
															<option value="">----Choose Bank----</option>
															
															<option value="SBI">SBI</option>
															</select>

													</div>
													<div class="form-group">
													<label>Challan Comment</label> <input type="text"
														id="challan_comment" name="challan" class="form-control"
														placeholder="Remarks">
												</div>
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" name="submitChallan" value="Submit"
															onclick="makePayment('challan')"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>


							<div id="step-5">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Refund Payment</h4>
										<h3>Step 2</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>Cheque</h3>
												</div>

												<div class="form-group has-feedback">
													<input type="text" id="cheque_amount" name="amountDD"
														class="form-control" placeholder="Amount">
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Payee : abcde</label>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Payable At :
														Jaipur</label>
												</div>

												<div class="form-group has-feedback">
													<input type="text" name="cheque" class="form-control"
														placeholder="Cheque No." id="cheque_no">
												</div>

												<div class="form-group has-feedback">
													<div class="input-group">
														<label>DD Date</label> <input type="text"
															name="dateofCheque" id="cheque_date" class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>
												</div>

												<div class="form-group has-feedback">
													<select class="form-control" name="bank_list"
														id="cheque_bank">
														<option value="">----Choose Bank----</option>
														<option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option>
<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>

													</select>
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" name="submitCheque"
															onclick="makePayment('cheque')" value="Submit"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>


							<div id="step-6">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Update your profile</h4>
										<h3>Step 3</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>Net Banking</h3>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Beneficiary
														Account : 120000</label>
												</div>

												<div class="form-group has-feedback">
													<input type="text" name="ifsc" class="form-control"
														placeholder="IFSC Code">
												</div>

												<div class="form-group has-feedback">
													<div class="input-group">
														<input type="text" name="tid" class="form-control"
															placeholder="Transaction ID">
													</div>
												</div>

												<div class="form-group has-feedback">
													<select class="form-control" name="bank_list">
														<option value="">----Choose Bank----</option>
														<option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option>
<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>

													</select>
												</div>

												<div class="form-group has-feedback">
													<input type="text" name="amountNetBanking"
														class="form-control" placeholder="Amount">
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="button" name="submitCheque" value="Submit"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>



							<div id="step-7">
								<div class="box box-primary">
									<!-- /.register-box -->
									<div class="register-logo">
										<a href="#"><b>ERP</b></a>
										<h4>Fee Payment</h4>
										<h3>Step 3</h3>
									</div>
									<div class="register-box">
										<div class="register-box-body">
											<form action="" method="post">
												<div class="form-group">
													<h3>NEFT</h3>
												</div>

												<div class="form-group">
													<label for="exampleInputPassword1">Beneficiary
														Account : 120000</label>
												</div>
												<div class="form-group has-feedback">
													<div class="input-group">
														<label>NEFT Date</label> <input type="text"
															name="dateofCheque" id="neft_date" class="form-control"
															data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
													</div>
												</div>

												<div class="form-group has-feedback">
													<label>IFSC Code</label> <input type="text" id="ifsc"
														name="ifsc" class="form-control" placeholder="IFSC Code">
												</div>

												<div class="form-group has-feedback">
													<div class="input-group">
														<label>Transaction Id</label> <input type="text" id="tid"
															name="tid" class="form-control"
															placeholder="Transaction ID">
													</div>
												</div>

												<div class="form-group has-feedback">
													<label>Choose bank</label> <select class="form-control"
														id="bank" name="bank_list" id="neft_bank">
														<option value="">----Choose Bank----</option>
														<option value="Allahabad Bank" >Allahabad Bank</option>
<option value="Andhra Bank" >Andhra Bank</option>
<option value="Axis Bank" >Axis Bank</option>
<option value="Bandhan Bank" >Bandhan Bank</option>
<option value="Bank of Baroda" >Bank of Baroda</option>
<option value="Bank of India" >Bank of India</option>
<option value="Bank of Maharashtra" >Bank of Maharashtra</option>
<option value="Bharatiya Mahila Bank" >Bharatiya Mahila Bank</option>
<option value="Canara Bank" >Canara Bank</option>
<option value="Catholic Syrian Bank" >Catholic Syrian Bank</option>
<option value="Central Bank of India" >Central Bank of India</option>
<option value="City Union Bank" >City Union Bank</option>
<option value="Corporation Bank" >Corporation Bank</option>
<option value="DCB Bank" >DCB Bank</option>
<option value="Dena Bank" >Dena Bank</option>
<option value="Dhanlaxmi Bank" >Dhanlaxmi Bank</option>
<option value="Federal Bank" >Federal Bank</option>
<option value="HDFC Bank" >HDFC Bank</option>
<option value="ICICI Bank" >ICICI Bank</option>
<option value="IDBI Bank" >IDBI Bank</option>
<option value="Indian Bank" >Indian Bank</option>
<option value="Indian Overseas Bank" >Indian Overseas Bank</option>
<option value="ING Vysya Bank" >ING Vysya Bank</option>
<option value="Jammu and Kashmir Bank" >Jammu and Kashmir Bank</option>
<option value="Karur Vysya Bank" >Karur Vysya Bank</option>
<option value="Kotak Mahindra Bank" >Kotak Mahindra Bank</option>
<option value="Lakshmi Vilas Bank" >Lakshmi Vilas Bank</option>
<option value="Nainital Bank" >Nainital Bank</option>
<option value="Oriental Bank of Commerce" >Oriental Bank of Commerce</option>
<option value="Punjab & Sind Bank" >Punjab & Sind Bank</option>
<option value="Punjab National Bank" >Punjab National Bank</option>
<option value="RBL Bank" >RBL Bank</option>
<option value="South Indian Bank" >South Indian Bank</option>
<option value="State Bank of Bikaner and Jaipur" >State Bank of Bikaner and Jaipur</option>
<option value="State Bank of Hyderabad" >State Bank of Hyderabad</option>
<option value="State Bank of India The Good Bank" >State Bank of India The Good Bank</option>
<option value="State Bank of Mysore" >State Bank of Mysore</option>
<option value="State Bank of Patiala" >State Bank of Patiala</option>
<option value="State Bank of Travancore" >State Bank of Travancore</option>
<option value="Syndicate Bank" >Syndicate Bank</option>
<option value="Tamilnad Mercantile Bank" >Tamilnad Mercantile Bank</option>
<option value="UCO Bank" >UCO Bank</option>
<option value="Union Bank of India" >Union Bank of India</option>
<option value="United Bank of India" >United Bank of India</option>
<option value="Vijaya Bank" >Vijaya Bank</option>
<option value="Yes bank" >Yes bank</option>


													</select>
												</div>

												<div class="form-group has-feedback">
													<label>Amount</label> <input type="text" id="neft_amount"
														name="amountNEFT" class="form-control"
														placeholder="Amount">
												</div>

												<div class="row">
													<div class="col-xs-4">
														<input type="button" onclick="back()" value="Prev"
															class="btn btn-warning btn-block btn-flat" />
													</div>
													<div class="col-xs-4 pull-right">
														<input type="submit" name="submitCheque" value="Submit"
															onclick="makePayment('neft')"
															class="btn btn-primary btn-block btn-flat" />
													</div>
													<!-- /.col -->
												</div>

											</form>
										</div>
										<!-- /.form-box -->
									</div>


								</div>

							</div>


						</div>
						<div class="col-md-3"></div>

					</div>
				</div>
			</section>
			<!-- /.content -->
		</div>
	</div>


	<script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<script>
		$(function() {
			//Datemask dd/mm/yyyy
			$("#datemask").inputmask("dd/mm/yyyy", {
				"placeholder" : "dd/mm/yyyy"
			});
			//Datemask2 mm/dd/yyyy
			$("#datemask2").inputmask("mm/dd/yyyy", {
				"placeholder" : "mm/dd/yyyy"
			});
			//Money Euro
			$("[data-mask]").inputmask();
		});
	</script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="../plugins/morris/morris.min.js"></script>
	<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../plugins/knob/jquery.knob.js"></script>
	<script src="../plugins/input-mask/jquery.inputmask.js"></script>
	<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="../plugins/daterangepicker/daterangepicker.js"></script>
	<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../plugins/fastclick/fastclick.min.js"></script>
	<script src="../dist/js/app.min.js"></script>
	<script src="../dist/js/pages/dashboard.js"></script>
	<script src="../dist/js/demo.js"></script>
	<script>
	function makePayment(method) {
	var transaction_id=document.getElementById("transaction_id").value;
		if (method == "dd") {
			amount = document.getElementById('dd_amount').value;
			payment_method = "demand_draft";
			date = document.getElementById('dd_date').value;
			bank = document.getElementById('dd_bank').value;
			ref_no = "dd_no=" + document.getElementById('dd_no').value;
		}

		if (method == "neft") {
			amount = document.getElementById('neft_amount').value;
			payment_method = "neft";
			date = "";
			temp1 = document.getElementById('neft_bank').value;
			temp2 = document.getElementById('ifsc').value;
			;
			bank = temp1 + " " + temp2;
			ref_no  = "tid=" + document.getElementById('tid').value;
		}
		if (method == "challan") {
			amount = document.getElementById('challan_amount').value;
			payment_method = "challan";
			date = document.getElementById('challan_date').value;
			bank = "";
			ref_no  = "challan_comment=" + document.getElementById('challan_comment').value;
		}
		var xmlhttp;
		try {
			xmlhttp = new XMLHttpRequest();
		} catch (e) {
			// Internet Explorer Browsers
			try {
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					// Browser doesn't support ajax
					alert("Your browser is unsupported");
				}
			}
		}

		if (xmlhttp) {
			xmlhttp.onreadystatechange = function() {

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	               alert("Payment Information saved successfully!");
	               window.location.href="../office/FeePaymentList.jsp?transaction_id="+transaction_id;
				}
				if (xmlhttp.status == 404)
					alert("Could not connect to server");
			}
			
			xmlhttp.open("POST", "../AddOfficePaymentData", true);
			xmlhttp.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
			xmlhttp.send("refund=true&"+"amount=" + amount + "&date=" + date + "&payment_method="
					+ payment_method + "&bank=" + bank + "&" +ref_no +"&transaction_id="+transaction_id);

		}
	}

	</script>
</body>

</html>